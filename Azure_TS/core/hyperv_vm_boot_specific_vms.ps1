﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify common string name of the VMs you need to configure or leave it blank to select all running VMs")] [AllowEmptyString()] [string]$VMName,
    [Parameter(Mandatory=$true, HelpMessage="Specify 'r' for restart test or 's' for shutdown/startup test")] [string]$testtype,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig,
    [Parameter(Mandatory=$true, HelpMessage="Wait time in between execution (in seconds)")] 
    [ValidateRange(1, [int]::MaxValue)]
        [int]$timer
)

$global:root = (Get-Item $PSScriptRoot).FullName
$global:mydoc = [environment]::getfolderpath("mydocuments")
$testVMs = "$mydoc\testvms.txt"
$vmarray = new-object system.collections.arraylist
$selectarray = new-object system.collections.arraylist

"`nPlease enter Test VM credentials`n"
$global:cred = Get-Credential -Message 'Test VM' -UserName pvteam64

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\azure_func.psm1

if (Test-Path $testVMs)
{
    Remove-item -Path $testVMs -Force -ErrorAction SilentlyContinue
}


function robusttest()
{
    param([String[]] $test_vms)

    PoweronVMs $vmarray

    Wait-VMs -timeout "600" -VMs $vmarray

    If ($testtype -eq 'r')
    {
        foreach ($VM in $test_vms)
        {
            Enable-VMIntegrationService -VMName $VM -Name 'Guest Service Interface'
        }
    }

    "`nSetting up RPC`n"
    RPCsetup $vmarray $cred

    Sleep 5

    "`n`nChecking Guest Driver...`n"
    CheckAllGuestDriver -VMs $vmarray -Credential $cred

    $teststart = (Get-Date).TimeOfDay

    ##### Test specification and duration
    if ($testtype -eq "s") # shutdown
    {
        "`n`nStarting Cold boot test from Hypervisor for $loops loops"
        "----------------------------------------------------------------------------`n"
        for ($loop = 1; $loop -le $loops; $loop++)
        {
            "`n`nStaring iteration $loop`n"
            $whencheck = Get-Date

            StressVMs "Shut down" -Timeout "600" -Interval "2"

            Sleep $timer

            PoweronVMs $vmarray

            Wait-VMs -timeout "600" -VMs $vmarray

            Sleep 2

            CheckTDRonAllGuests $whencheck $vmarray $cred
            CheckAllGuestDriver -VMs $vmarray -Credential $cred

            Sleep 2
        }
    }
    elseif ($testtype -eq "r") # restart
    {    
        "`n`nStarting Warm boot test from Guest for $loops loops"
        "----------------------------------------------------------------------------`n"
        for ($loop = 1; $loop -le $loops; $loop++)
        {
            "`n`nStarting iteration $loop`n"
            $whencheck = Get-Date

            StressVMs "Restart" -Timeout "600" -Interval "2" #240 means 240s max wait time for OS to restart and 2 means check every 2 seconds
            
            Wait-VMs -timeout "600" -VMs $vmarray

            Sleep 2

            CheckTDRonAllGuests $whencheck $vmarray $cred
            CheckAllGuestDriver -VMs $vmarray -Credential $Cred

            Sleep 2
        }
    }
    else
    {
        Write-Error -Message "Wrong action $testtype. Please select either 'r' or 's'." -Category InvalidArgument -ErrorAction Stop 
    }

    $testend = (Get-Date).TimeOfDay
    "`nBoot test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes`n"
    "`nAll $loops loops have been completed. Please check mydocuments\ps_log\ for more details`n`n"

}


Function StressVMs
{
    param(
        [Alias('TestName')]
            [String] $arg1, 
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait, 
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime
    )

    if ($arg1 -eq "Restart") 
    {
        foreach ($VM in $test_vms)
        {  
            Invoke-Command -VMName $VM -ScriptBlock {shutdown /r /t 0} -Credential $cred -ErrorAction SilentlyContinue
            "Restart initiated on $VM`n"
            $selectarray.add($VM) | Out-Null
            Sleep $timer
        }

    } elseif ($arg1 -eq "Shut down") {

        foreach ($VM in $test_vms)
        {
            Stop-VM -Name $VM -AsJob:$true -Confirm:$false -Force
            $selectarray.add($VM) | Out-Null
            "Shutdown initiated on $VM`n"
            Sleep $timer
        }
    }

    #Wait until all VMs have restarted or shut down

    if ($arg1 -eq "Shut down") 
    {
        WaitforVMoff -VMList $selectarray -Interval $waittime -Timeout $maxwait
    
    } elseif ($arg1 -eq "Restart") {

        WaitforVMreboot -VMList $selectarray -Interval $waittime -Timeout $maxwait
    }
    $selectarray.clear()
}


If ([string]::IsNullOrEmpty($VMName)) {

    $vmnames = (Get-VM -Name * | where {$_.State -match "Running"}).Name #get all running VMs
    $affectedvms = "all running vms"

} else {

    $vmnames = (Get-VM -Name $VMName* | where {$_.State -match "Running"}).Name #get specified powered on vms
    $affectedvms = "all running vms with name starting $VMName"
}


foreach ($vm in $vmnames.Name)
{
    $vmarray.add($vm) | Out-Null
}

ShutdownVMs $vmarray

WaitforVMoff -VMList $vmarray -Interval "2" -Timeout "360"
$vmarray.Clear()

vfmultiassignment -Guests $vmnames -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

robusttest $vmnames
#Stop-Transcript

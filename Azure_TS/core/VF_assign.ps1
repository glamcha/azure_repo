﻿Param (
        [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] [ValidateSet("1", "2", "4", "8")] [int]$vfconfig,
        [Parameter(Mandatory=$true, HelpMessage="Specify common string name of the VMs you need to configure or leave it blank to select all available VMs")] [AllowEmptyString()] [string]$VM_Name,
        [Parameter(Mandatory=$true, HelpMessage="Density or Performance")] [string] $allocation,
        [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8G setting")] $resourceconfig
    )

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$availablegpus = (Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | measure).Count


If ([string]::IsNullOrEmpty($VM_Name)) {

    $vmnames = (Get-VM -Name *).Name #get all VMs

} else {

    $vmnames = (Get-VM -Name $VM_Name*).Name #get specified vms
}

vfassignment $vmnames $vfconfig $availablegpus 0 $allocation $resourceconfig

if ($rome64flag -eq 1)
{
    cpugroups $vmnames
}
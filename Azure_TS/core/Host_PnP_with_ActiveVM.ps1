﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$root = (get-item $PSScriptRoot).parent.FullName
$generation = 1

"`nPlease enter Test VM credentials`n"
$global:cred = Get-Credential -Message 'Test VM' -UserName amd\

if (Test-Path "$root\result.ini")
{
    Remove-Item "$root\result.ini"
}

function Start-ScanHardware{
    param($root)
    if((Get-WmiObject Win32_OperatingSystem).OSArchitecture -eq "64-bit")
    {
        $devcon = "$root\devcon\devcon64.exe"
    }
    else
    {
        $devcon = "$root\devcon\devcon.exe"
    }

    Start-Process -FilePath $devcon -ArgumentList "rescan" -NoNewWindow -Wait
}


"`nStarting Host PnP Test`n"

echo "[STEPS]" >> "$root\result.ini"
echo "`n" >> "$root\result.ini"
echo "Number=1" >> "$root\result.ini"
echo "`n" >> "$root\result.ini"

echo "[STEP_001]"  >> "$root\result.ini"

try{
    
    TestHostEnv

    testvmcreation -Gen $generation -Total 1 -VMName TestVM -Template $vmbaselocation
    
    if ($rome64flag -eq 1)
    {
        cpugroups TestVM1
    }

    Start-Guest TestVM1 -timeout "360"
    WaitforOS -timeout "600" -VM TestVM1

    "`nShutting down VM"
    Stop-VM -Name TestVM1 -Confirm:$false -Force
    
    Do
    {
        $vmstate = Get-VM -Name TestVM1
        Sleep 1
    } Until ($vmstate.State -ne "Running")

    $teststart = (Get-Date).TimeOfDay

    for($i = 1; $i -le $loops; $i++)
    {
        "`n`nRunning iteration $i"

        "Disabling device(s)"
        $devices = Get-AmdDevices
        $devices.disable() | Out-Null
        Start-ScanHardware $root| Out-Null

        $devices = Get-AmdDevices

        foreach($device in $devices)
        {
            if($device.ConfigManagerErrorCode -ne 22)
            {
                throw "!!!Error, failed to disable $($device.PNPDeviceID) returned errorCode $($device.ConfigManagerErrorCode)"
            }
        }
        "Disable success"

        Sleep 2

        "Enabling Device(s)"
        $devices.enable() | Out-Null
        Start-ScanHardware $root| Out-Null

        $devices = Get-AmdDevices

        foreach($device in $devices)
        {
            if($device.ConfigManagerErrorCode -ne 0)
            {
                throw "!!!Error, failed to enable $($device.PNPDeviceID) returned errorCode $($device.ConfigManagerErrorCode)"
            }
        } 

        "Device Enable Success`n"

        vfassignment -VMName TestVM1 -VF 4 -Adapter 1 -CustomList 0 -Allocation "Performance" -VMResource A
        $whencheck = Get-Date
        Start-VM -Name TestVM1 -Confirm:$false -ErrorAction SilentlyContinue
    
        Do 
        {
            $status = Get-VM -Name TestVM1
            Sleep 1
        } Until ($status.State -eq 'Running') 
               

        WaitforOS -timeout '600' -VM TestVM1


        $errorCode = Get-GuestDriverStatus -vmname TestVM1 -cred $cred

        If($errorCode -ne 0)
        {
            throw "!!!Error! Driver returned errorCode $errorCode"
        } else {
            "`nDriver is running properly`n"
        }

        CheckTDR $whencheck TestVM1 $cred

        "`nShutting down VM`n"
        Stop-VM -Name TestVM1 -Confirm:$false -Force
        do
        {
            $vmstate = Get-VM -Name TestVM1
            Sleep 1
        } Until ($vmstate.State -ne "Running")

    }
        
    echo "Status=Pass" >> "$root\result.ini"

    $testend = (Get-Date).TimeOfDay
    "Host PnP test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

}
catch{
    echo "Status=Fail" >> "$root\result.ini"
    echo "Description=$($_.Exception.Message)" >> "$root\result.ini"
    "`n!!!Host PnP Failed`n"
}

Stop-Transcript
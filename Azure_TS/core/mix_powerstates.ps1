﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string[]]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Is the template sysprepped (y/n)?")]
        [string]$sysprep,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] 
    [ValidateRange(1,8)]
        [int]$numofgpus,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig,
    [Parameter(Mandatory=$true, HelpMessage="Wait time in between execution (in seconds)")] 
    [ValidateRange(0, [int]::MaxValue)]
        [int]$timer
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$vmarray = new-object system.collections.arraylist
$selectarray = new-object system.collections.arraylist
$totalvms = $vfconfig * $numofgpus
$generation = 1


$RS5testVmName = "RS5-stresstest-"
$W19H1testVmName = "W19H1-stresstest-"
$EVDtestVmName = "EVD-stresstest-"
$W2019testVmName = "W2019-stresstest-"
$W2016testVmName = "W2016-stresstest-"
$W20H1testVmName = "W20H1-stresstest-"


"`nPlease enter Test VM credentials`n"
$global:cred = Get-Credential -Message 'Test VM' -UserName amd\

DefineTemplate $vmbaselocation


function mixps
{
    param([String[]] $test_vms)

    #$VMs = Get-VM | Where-Object {$_.Name -match $test_vms}

    Sleep 2

    PoweronVMs $vmarray

    Wait-VMs -timeout "600" -VMs $vmarray

    "`nPrepping VMs`n"
    RPCsetup $vmarray $cred
    DisableVMRecovery $vmarray $cred
    
    Sleep 2

    "`nChecking Guest Driver`n"
    CheckAllGuestDriver -VMs $vmarray -Credential $cred

    $teststart = (Get-Date).TimeOfDay

    "`n`nStarting VM Mix Power States test for $loops loops"
    "----------------------------------------------------------------------------`n"
    for ($loop = 1; $loop -le $loops; $loop++)
    {
        "`n`nStaring iteration $loop`n"
        $whencheck = Get-Date

        StressVMs -TestName "Mix Powerstates" -Timeout "600" -Interval "2"

        Sleep $timer

        WaitandPowerON -VMList $selectarray -Interval "2" -Timeout "600"

        Wait-VMs -timeout "600" -VMs $vmarray

        Sleep 2

        CheckTDRonAllGuests $whencheck $vmarray $cred
        CheckAllGuestDriver -VMs $vmarray -Credential $cred

        Sleep 2

        $selectarray.Clear()
    }

    $testend = (Get-Date).TimeOfDay
    "Mix Powerstates test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

    "`nAll $loops loops have been completed. Please check mydocuments\ps_log\ for more details`n`n"

}


Function StressVMs
{
    param(
        [Alias('TestName')]
            [String] $arg1, 
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait, 
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime
    )

    "VM $arg1 test to start"

    foreach ($VM in $test_vms)
    {

        $random = Get-Random -Maximum 3

        if ($random -eq 0)
        {
            #Guest reboot
            Start-Job -Name $VM -ScriptBlock {param($VM,$cred) Invoke-Command -VMName $VM -ScriptBlock {shutdown /r /t 0} -Credential $cred} -ArgumentList $VM,$cred
            Sleep $timer
        } elseif ($random -eq 1)
        {
            #shutdown
            Start-Job -Name $VM -ScriptBlock {param($VM) Stop-VM -Name $VM -Confirm:$false -Force} -ArgumentList $VM
            $selectarray.add($VM) | Out-Null
            Sleep $timer
        } elseif ($random -eq 2)
        {
            #shutdown from guest
            Start-Job -Name $VM -ScriptBlock {param($VM,$cred) Invoke-Command -VMName $VM -ScriptBlock {shutdown /s /t 0} -Credential $cred} -ArgumentList $VM,$cred
            $selectarray.add($VM) | Out-Null
            Sleep $timer
        } elseif ($random -eq 3)
        {
            #force off
            Start-Job -Name $VM -ScriptBlock {param($VM) Stop-VM -Name $VM -Confirm:$false -Turnoff} -ArgumentList $VM
            $selectarray.add($VM) | Out-Null
            Sleep $timer
        } elseif ($random -eq 4)
        {
            #force reset
            Start-Job -Name $VM -ScriptBlock {param($VM) Restart-VM -Name $VM -Confirm:$false -Force} -ArgumentList $VM
            Sleep $timer
        }
    }

}

multivmoscreation -Gen $generation -Total $totalvms `
                  -RS5baselocation $RS5Template -RS5testVmName $RS5testVmName `
                  -W19H1baselocation $W19H1Template -W19H1testVmName $W19H1testVmName `
                  -W2019baselocation $W2019Template -W2019testVmName $W2019testVmName `
                  -EVDbaselocation $EVDTemplate -EVDtestVmName $EVDtestVmName `
                  -W2016baselocation $W2016Template -W2016testVmName $W2016testVmName `
                  -W20H1baselocation $W20H1Template -W20H1testVmName $W20H1testVmName


Foreach ($testname in $vmnames)
{
    Invoke-Expression "$root\core\VM_Hibernation_Enable.ps1 $testname"
}

foreach ($vm in $vmnames)
{
    $vmarray.add($vm) | Out-Null
}

if ($sysprep -like 'y*') 
{
    WaitforSysprep $vmarray
}

vfmultiassignment -Guests $vmnames -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig

if ($rome64flag -eq 1)
{
    cpugroups $vmnames
}

mixps $vmnames

Stop-Transcript

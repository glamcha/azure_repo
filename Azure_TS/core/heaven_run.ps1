﻿#Run this in Administrator if restriction is enabled
#Set-ExecutionPolicy Unrestricted -Force

Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run test")]
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops,
    [Parameter(Mandatory=$true, HelpMessage="Specify which API to test")] 
    [ValidateSet("direct3d11", "direct3d9", "opengl", "all")]
        [string[]]$apis
)

#$session = "PCoIP Session Imaging Statistics(PCoIP Session 1 pvteam64)"
$timestamp = $([DateTime]::Now).ToString()
$time = Get-Date -format g | foreach {$_ -replace ":", "_"} | foreach {$_ -replace "/", "_"} | foreach {$_ -replace " ", "__"}
$logfile=$PSScriptRoot+ '\results_' + $time +".csv"
$currentloc = (Get-Location).Path
$engine_log_caption='FPS,Score,Min,Max,API,Resolution'
$engine_log_format='$F,$S,$z,$x,$A,$v'
#$bin = $PSScriptRoot + '/bin'
$bin = "C:\Heaven\bin"
$checkos = ([System.Environment]::OSVersion.Version).Build

[int]$width='1920'
[int]$height='1080'

Remove-item -Path "$PSScriptRoot\frames_log.csv" -ErrorAction SilentlyContinue
Add-Content $logfile $timestamp
Add-Content $logfile $engine_log_caption

cd $bin

if ($apis -eq 'all')
{
    $apis = New-Object System.Collections.ArrayList
    $apis = @('direct3d11', 'direct3d9', 'opengl')
}

foreach ($api in $apis)
{
    for ($loop = 1; $loop -le $loops; $loop++)
    {
            #start-process -Wait -filepath Heaven.exe -ArgumentList '-video_app $api -sound_app openal -project_name "Heaven" -data_path ../ -system_script heaven/unigine.cpp -engine_config ../data/heaven_4.0.cfg -video_multisample 0 -video_fullscreen 0 -video_mode -1 -video_width $width -video_height $height -frame -1 -duration 0 -type 0 -level 0 -extern_define RELEASE,HEAVEN_ADV,AUTOMATION,QUALITY_HIGH,TESSELLATION_DISABLED -extern_plugin "GPUMonitor" -scores "$bin\api.tmp" -format "$engine_log_format"'
        .\Heaven.exe `
        -video_app $api `
        -sound_app openal `
        -project_name "Heaven" `
        -data_path ../ `
        -system_script heaven/unigine.cpp `
        -engine_config ../data/heaven_4.0.cfg `
        -video_multisample 0 `
        -video_fullscreen 0 `
        -video_mode -1 `
        -video_width $width `
        -video_height $height `
        -frame -1 `
        -duration 0 `
        -type 0 `
        -level 0 `
        -extern_define 'RELEASE,HEAVEN_ADV,AUTOMATION,QUALITY_HIGH,TESSELLATION_DISABLED' `
        -scores "$bin\api.tmp" `
        -format "$engine_log_format" `
        -frames "$PSScriptRoot\frames_log.csv"
            
        sleep 15
            
        #Start-Job -Name perfcounters -ScriptBlock {param($currentloc, $session) typeperf -o $currentloc\counters.csv -y "\\$env:COMPUTERNAME\$session\Imaging Encoded Frames/sec"} -ArgumentList $currentloc,$session -ErrorAction SilentlyContinue
            
        $unigine = Get-Process Heaven
        Wait-Process -Id $unigine.id    

        #Stop-Process -name typeperf -Force -ErrorAction SilentlyContinue

        type "$bin\api.tmp" >> $logfile

        <#If (Test-Path $PSScriptRoot\counters.csv)
        {
            $counters = @(Import-Csv $PSScriptRoot\counters.csv -ErrorAction SilentlyContinue | select -ExpandProperty "*Encoded*")
            $counters | Measure-Object -Average -Minimum -Maximum | Select Average,Minimum,Maximum -ErrorAction SilentlyContinue >> $logfile
        } else {
            "`nNo counters available with this protocol"
            "No encoded fps can be calculated"
        }#>
        Remove-item -Path "$bin\api.tmp" -ErrorAction SilentlyContinue
        #Remove-item -Path "$PSScriptRoot\counters.csv" -ErrorAction SilentlyContinue
    }  
}
$apis.clear()
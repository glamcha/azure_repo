Param(
    [Parameter(Mandatory=$true, HelpMessage="Specify VM name")]
    [ValidateNotNullOrEmpty()]
    [string]$VmName
)

function Get-WMIVM
{
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$VmName = ""
        )

    gwmi -namespace root\virtualization\v2 -query "select * from Msvm_ComputerSystem where ElementName = '$VmName'"
}

function Get-WMIVmSettingData
{
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$VmName = ""
        )
    $vm = Get-WMIVM $VmName

    return $vm.GetRelated("Msvm_VirtualSystemSettingData","Msvm_SettingsDefineState",$null,$null, "SettingData", "ManagedElement", $false, $null)
}

$enabled = $true
$vmms = Get-WmiObject -Namespace "root\virtualization\v2" -Class Msvm_VirtualSystemManagementService
$vssd = Get-WmiVmSettingData $VmName
Write-Output ("`nEnable Hibernation " + $vssd.EnableHibernation)
$vssd.EnableHibernation = $Enabled
$settingsText = $vssd.PSBase.GetText("CimDtd20")
$ret = $vmms.ModifySystemSettings($settingsText)
if ($ret.ReturnValue -eq 0) {
   Write ("Hibernation enabled on $VmName`n")
} else {
   Write ("Failed to enable hibernation. Status:" + $ret.ReturnValue)
}

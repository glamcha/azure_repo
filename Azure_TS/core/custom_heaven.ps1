﻿Param (
    #[Parameter(Mandatory=$true, HelpMessage="Specify common string name of the VMs you need to configure or leave it blank to select all running VMs")] [AllowEmptyString()] [string]$VMName,
    #[Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] [string]$global:vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] [int]$numofloops,
    [Parameter(Mandatory=$true, HelpMessage="Specify name of log folder on loginvsi")] [string]$logfoldername,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] $resourceconfig
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

[string[]]$apitest = (Read-Host -Prompt "Choose from ('dx11' 'dx9' 'ogl') (separate them by comma)").split(',') | % {$_.trim()}
$logfile = "$mydoc\heaven_" + $time +"_log.txt"
$vmarray = new-object system.collections.arraylist
$vmcheck = new-object system.collections.arraylist
$curTime = (Get-Date).ToString("MMddyyyyHHmm")
$heavenVMs = $customvms.custom_vm_list
$numofgpus = [int]($heavenVMs.Count/$vfconfig)

[System.Collections.Hashtable]$customvms = Import-PowerShellDataFile "$root\customvms.psd1"

PrepRDP

"`nPlease enter VM credentials`n"
$cred = Get-Credential -Message VM -UserName AMD\

vfassignment $heavenVMs $vfconfig $numofgpus 1 1 $resourceconfig

foreach ($vm in $heavenVMs.Name)
{
    $vmarray.add($vm) | Out-Null
}

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

AddInternalNetwork $vmarray

PoweronVMs $vmarray
Wait-VMs -timeout "360" -VMs $vmarray

CopyHeaven $vmarray $cred $heavenlocation 0

Sleep 3

RDPConnect $vmarray $cred

Sleep 30



$apitest = $apitest.Replace('dx11','direct3d11')
$apitest = $apitest.Replace('dx9','direct3d9')
$apitest = $apitest.Replace('ogl','opengl')

$teststart = (Get-Date).TimeOfDay

for ($loop = 1; $loop -le $numofloops; $loop++) 
{
    "`n`nLoop $loop`n"

    for ($apirun = 0; $apirun -lt $apitest.count; $apirun++)
    {
        "`nLaunching Heaven $($apitest[$apirun]) benchmark`n"

        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid, $apitest, $apirun) PsExec64 -s -i $checkrdpid -d -accepteula 'C:\Heaven\apitest1.bat' $apitest[$apirun]} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid,$apitest,$apirun
        }

        Sleep 5
        $whencheck = Get-Date

        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $waitforcompletion = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
        
            If (!$waitforcompletion)
            {
                "Benchmark failed to run in $heavenVM"
                Exit
            } else
            {
                "Benchmark currently running"
                $vmcheck.add($heavenVM) | Out-Null
            }
        }

        "`nChecking for benchmark completion`n"

        $count = 1
        Do 
        {
            for ($x = 0; $x -lt $vmcheck.count; $x++)
            {
                $waitforcompletion = Invoke-Command -VMName $vmcheck[$x] -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
                if (!$waitforcompletion)
                {
                    $targetvm = $vmcheck[$x]
                    "$targetvm - Benchmark completed after $(10*$count) seconds"
                    $vmcheck.RemoveAt($x)
                    $x--
                ##TODO: Need to add result check for benchmark completion
                }
            }

            Sleep 10

            $count++

        } until (($vmcheck.count -eq 0) -or ($count -gt 90))

        If ($count -gt 90)
        {
            "`nChecking for TDR during benchmark run`n"
            #CheckTDR $whencheck $vmarray $cred
            CheckTDRonAllGuests $whencheck $vmarray $cred

            "`n`nChecking Guest Driver status...`n"
            CheckAllGuestDriver -VMs $vmarray -Credential $cred 

            foreach ($failvm in $vmcheck)
            { 
                Write-Host "Benchmark timed out in $failvm - 15 minutes. Please check $failvm!" -BackgroundColor Red 
            } 
            "FAILED: Only $loop loops out of $numofloops completed" 
            Exit
        }
        $vmcheck.Clear()
    }
}

$testend = (Get-Date).TimeOfDay
"Heaven test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

"`nCopying Results to \\loginvsi\Logs\Azure\Automated_Results\HeavenAPI\$logfoldername`n"

foreach ($heavenVM in $heavenVMs.Name) {

    Invoke-Command -VMName $heavenVM -ScriptBlock {param($heavenVM, $logfoldername)
    $ip = (Get-NetIPAddress -AddressFamily IPv4 | where {($_.InterfaceAlias -notmatch 'Loopback') -and ($_.PrefixOrigin -like 'Dhcp')}).IPAddress | select -first 1
        new-item -itemtype directory -force -path \\loginvsi\logs\azure\Automated_Results\HeavenAPI\$logfoldername\$heavenVM
        Copy-Item -Path C:\Heaven\api_run.csv -Destination \\loginvsi\Logs\Azure\Automated_Results\HeavenAPI\$logfoldername\$heavenVM\heaven_api.csv -Force
        Rename-Item -Path C:\Heaven\api_run.csv -NewName C:\Heaven\results_old.csv -ErrorAction SilentlyContinue -Force 
    } -credential $cred -ArgumentList $heavenVM,$logfoldername
 
}

Stop-Transcript
﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$testVMs = "$mydoc\testvms.txt"
$vmarray = new-object system.collections.arraylist
$selectarray = new-object system.collections.arraylist
$testVmName = "Hibernate-stresstest-"
$totalvms = 1
$generation = 1


"`nPlease enter Test VM credentials`n"
$global:cred = Get-Credential -Message 'Test VM' -UserName AMD\taccuser	


function robusttest()
{
    param([String] $test_vms)

    foreach ($vm in $test_vms)
    {
        $vmarray.add($vm) | Out-Null
    }

    Sleep 2

    if ($rome64flag -eq 1)
    {
        cpugroups $vmarray
    }

    PoweronVMs $vmarray

    Wait-VMs -timeout "600" -VMs $vmarray

    Sleep 5

    "`nSetting up RPC`n"
    RPCsetup $vmarray $cred

    Sleep 2

    "`nChecking Guest Driver`n"
    CheckAllGuestDriver -VMs $vmarray -Credential $cred
    
    $teststart = (Get-Date).TimeOfDay
    
    "`n`nStarting VM Hibernation for $loops loops"
    "----------------------------------------------------------------------------`n"
    for ($loop = 1; $loop -le $loops; $loop++)
    {
        "`n`nStaring iteration $loop`n"
        $whencheck = Get-Date

        StressVMs "Hibernate" -Timeout "600" -Interval "2"
        PoweronVMs $vmarray

        Wait-VMs -timeout "600" -VMs $vmarray

        sleep 2

        CheckTDRonAllGuests $whencheck $vmarray $cred
        CheckAllGuestDriver -VMs $vmarray -Credential $cred

        Sleep 2
    }

    $testend = (Get-Date).TimeOfDay
    "VM Hibernation test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

    "`nAll $loops loops have been completed. Please check mydocuments\ps_log\ for more details`n`n"
}


Function StressVMs
{
    param(
        [Alias('TestName')]
            [String] $arg1, 
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait, 
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime
    )

    "VM $arg1 test to start"
    foreach ($VM in $VMs.Name)
    {
        Invoke-Command -VMName $VM -ScriptBlock {shutdown -h} -Credential $cred -ErrorAction SilentlyContinue
        "Hibernate initiated on $VM"
        $selectarray.Add($VM)
    }

    #Wait until all VMs have Hibernated
    WaitforVMHibernation -VMList $selectarray -Interval $waittime -Timeout $maxwait
    
    $selectarray.clear()
}

CheckVHD $vmbaselocation $vfconfig

testvmcreation $generation $totalvms $testVmName $vmbaselocation

$vmNamewithNum = $testVmName+"1"
Invoke-Expression "$root\core\VM_Hibernation_Enable.ps1 $vmNamewithNum"

vfassignment $testVmName $vfconfig 1 0 1 $resourceconfig
robusttest $testVmName
Stop-Transcript
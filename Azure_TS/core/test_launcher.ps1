﻿param( [Parameter(Mandatory=$true)][ValidateNotNull()] [int] $user_select)

$availablegpus = (Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | measure).Count
$possiblevf = (Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first 1).ValidPartitionCounts | Sort-Object


function test_case_id()
{
    if ($user_select -eq '1') {
    "`n`nVM Info`n"
    $servername = (Get-WmiObject Win32_OperatingSystem).CSName
    Get-VMInfo -servername $servername
    } 


    if ($user_select -eq '2') {
    "`n`nVM Cloning`n"
    Invoke-expression "$root\core\vm_clone.ps1"
    }   
    

    if ($user_select -eq '3') {
    "`n`nGet GPU Partition`n"
    Get-GPU
    }
    

    if ($user_select -eq '4') {
    "`n`nSet GPU Partition`n"
    Set-GPUPartition
    } 


    if ($user_select -eq '5') {
    "`n`n`nVF/VM Assignment`n"

    Write-Host "1. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "2. Enter VM Name to do the assignment on (Blank to select all available VMs or specific string to select specific VMs"
    Write-Host "3. Enter allocation type (Density (0) or Performance (1))"
    Write-Host "4: Resource: Select Azure default memory or 8GB config [Az or 8G]`n"
    
    Invoke-expression "$root\core\VF_assign.ps1"
    }


    if ($user_select -eq '6') {
    "`n`n[TESTCASE] VM Cold boot test from Host`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the vhd location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    $global:testtype = "s"
    Invoke-expression "$root\core\hyperv_vm_boot_v5.ps1"
    }
    

    if ($user_select -eq '7') {
    "`n`n[TESTCASE] VM Warm boot test from Guest`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    $global:testtype = "r"
    Invoke-expression "$root\core\hyperv_vm_boot_v5.ps1"
    }


    if ($user_select -eq '8') {
    "`n`n[TESTCASE] VM Cold boot test from Guest`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    Invoke-expression "$root\core\vm_shutdown_from_guest_v2.ps1"
    }


    if ($user_select -eq '9') {
    "`n`n[TESTCASE] VM Reset`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    Invoke-expression "$root\core\vm_reset_v2.ps1"
    }


    if ($user_select -eq '10') {
    "`n`n[TESTCASE] VM Poweroff`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    Invoke-expression "$root\core\vm_poweroff_v2.ps1"
    }


    if ($user_select -eq '11') {
    "`n`n[TESTCASE] Heaven API test`n"

    cmdkey /add:loginvsi /user:amd\taccuser /pass:AH64_uh1 | Out-Null

    Write-Host "Please choose between Selective VMs (s) or Automated VMs (a)"
    [string] $option_heaven = Read-Host -Prompt "Choose from ('selective' 'automated')"

        if ($option_heaven -like 'a') {

        Write-Host "`n1. vmbaselocation: Specify the virtual hard disk location of the template"
        Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
        Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
        Write-Host "4. loops: Enter number of loops to complete"
      
        Write-Host "5. Specify log folder name in loginvsi"
        Write-Host "6: Resource: Select Azure default memory or 8GB config [Az or 8G]`n"

        Write-Host "7. Specify which heaven api to run`n"

        preparesys
        checktemploc
        Invoke-expression "$root\core\heaven_allapi.ps1"
        
        } elseif ($option_heaven -like 's') {

        Write-Host "`nPlease edit the customvms.psd1 in $root to choose your own VMs before continuing`n" -BackgroundColor DarkGreen

        #Write-Host "`n1. VMName: Specify common string name of the VMs you need to configure or leave it blank to select all running VMs"
        Write-Host "1. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
        Write-Host "2. loops: Enter number of loops to complete"
        Write-Host "3. Specify log folder name in loginvsi`n"
        Write-Host "4. Specify which heaven api to run"
        Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]`n"
          
        Invoke-expression "$root\core\custom_heaven.ps1"
        
        }
    }


    if ($user_select -eq '12') {
    "`n`n[TESTCASE] Dynamic VF Test`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "2. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "3. loops: Enter number of loops to complete"
    Write-Host "4: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "5: Heaven: Enter heaven local copy location. If not, leave it blank`n"

    Invoke-expression "$root\core\Gim_reloadv2.ps1"
    }


    if ($user_select -eq '13') {
    "`n`n[TESTCASE] Host Driver PnP`n"

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "2. loops: Enter number of loops to complete`n"

    Invoke-expression "$root\core\Host_PnP_with_ActiveVM.ps1"
    }

    if ($user_select -eq '14') {
    "`n`n[TESTCASE] Single VM Hibernation`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. loops: Enter number of loops to complete"
    Write-Host "4: Resource: Select Azure default memory or 8GB config [Az or 8G]`n"

    Invoke-expression "$root\core\single_vm_hibernate.ps1"
    }

    if ($user_select -eq '15') {
    "`n`n[TESTCASE] VM Hibernation`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    Invoke-expression "$root\core\hyperv_vm_hibernate_all.ps1"
    }

    if ($user_select -eq '16') {
    "`n`n[TESTCASE] VM Mix Power States`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"
    Write-Host "6: Timer: Select a wait time in between execution (in seconds) - 0 for no wait time`n"

    Invoke-expression "$root\core\mix_powerstates.ps1"
    }

    if ($user_select -eq '17') {
    "`n`n[TESTCASE] Force App Kill`n"

    Write-Host "`n[IMPORTANT] Make sure your VM template is Generation 1 and 'AMD VM Testers' are already added as admins" -ForegroundColor Black -BackgroundColor white

    Write-Host "1. vmbaselocation: Specify the virtual hard disk location of the template"
    Write-Host "   If you need to test multiple OSes, add to the next line of vmbaselocation"
    Write-Host "   Leave it blank and Press Enter on the next line to complete your selections`n"
    Write-Host "2. vfconfig: Select VF configuration" "(Possible: $possiblevf)"
    Write-Host "3. numofgpus: Select how many gpus to be run on" "(Available: $availablegpus)"
    Write-Host "4. loops: Enter number of loops to complete"
    Write-Host "5: Resource: Select Azure default memory or 8GB config [Az or 8G]"

    Invoke-expression "$root\core\Force_App_Kill.ps1"
    }

}

test_case_id $user_select
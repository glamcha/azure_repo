﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string[]]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Is the template sysprepped (y/n)?")]
        [string]$sysprep,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] 
    [ValidateRange(1,8)]
        [int]$numofgpus,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$loops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig,
    [Parameter(Mandatory=$true, HelpMessage="Wait time in between execution (in seconds)")] 
    [ValidateRange(0, [int]::MaxValue)]
        [int]$timer
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$testVMs = "$mydoc\testvms.txt"
$vmarray = new-object system.collections.arraylist
$selectarray = new-object system.collections.arraylist
$totalvms = $vfconfig * $numofgpus
$generation = 1


$RS5testVmName = "RS5-stresstest-"
$W19H1testVmName = "W19H1-stresstest-"
$EVDtestVmName = "EVD-stresstest-"
$W2019testVmName = "W2019-stresstest-"
$W2016testVmName = "W2016-stresstest-"
$W20H1testVmName = "W20H1-stresstest-"


"`nPlease enter Test VM credentials`n"
$global:cred = Get-Credential -Message 'Test VM' -UserName amd\

DefineTemplate $vmbaselocation


function robusttest()
{
    param([String[]] $test_vms)

    PoweronVMs $vmarray

    Wait-VMs -timeout "600" -VMs $vmarray

    "`nSetting up RPC`n"
    RPCsetup $vmarray $cred

    Sleep 5

    "`n`nChecking Guest Driver...`n"
    CheckAllGuestDriver -VMs $vmarray -Credential $cred

    $teststart = (Get-Date).TimeOfDay

    ##### Test specification and duration
    if ($testtype -eq "s") # shutdown
    {
        "`n`nStarting Cold boot test from Hypervisor for $loops loops"
        "----------------------------------------------------------------------------`n"
        for ($loop = 1; $loop -le $loops; $loop++)
        {
            "`n`nStaring iteration $loop`n"
            $whencheck = Get-Date

            StressVMs "Shut down" -Timeout "600" -Interval "2"

            Sleep $timer

            PoweronVMs $vmarray

            Wait-VMs -timeout "600" -VMs $vmarray

            Sleep 2

            CheckTDRonAllGuests $whencheck $vmarray $cred
            CheckAllGuestDriver -VMs $vmarray -Credential $cred

            Sleep 2
        }

    }
    elseif ($testtype -eq "r") # restart
    {    
        "`n`nStarting Warm boot test from Guest for $loops loops"
        "----------------------------------------------------------------------------`n"
        for ($loop = 1; $loop -le $loops; $loop++)
        {
            "`n`nStarting iteration $loop`n"
            $whencheck = Get-Date

            StressVMs "Restart" -Timeout "600" -Interval "2" #240 means 240s max wait time for OS to restart and 2 means check every 2 seconds
            
            Wait-VMs -timeout "600" -VMs $vmarray

            Sleep 2

            CheckTDRonAllGuests $whencheck $vmarray $cred
            CheckAllGuestDriver -VMs $vmarray -Credential $Cred

            Sleep 2
        }
    }
    else
    {
        Write-Error -Message "Wrong action $testtype. Please select either 'r' or 's'." -Category InvalidArgument -ErrorAction Stop 
    }

    $testend = (Get-Date).TimeOfDay
    "`nBoot test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes`n"
    "`nAll $loops loops have been completed. Please check mydocuments\ps_log\ for more details`n`n"
}


Function StressVMs
{
    param(
        [Alias('TestName')]
            [String] $arg1, 
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait, 
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime
    )

    if ($arg1 -eq "Restart") 
    {
        foreach ($VM in $test_vms)
        {  
            Invoke-Command -VMName $VM -ScriptBlock {shutdown /r /t 0} -Credential $cred -ErrorAction SilentlyContinue
            "Restart initiated on $VM`n"
            $selectarray.add($VM) | Out-Null
            Sleep $timer
        }

    } elseif ($arg1 -eq "Shut down") {

        foreach ($VM in $test_vms)
        {
            Stop-VM -VMName $VM -AsJob:$true -Confirm:$false -Force
            $selectarray.add($VM) | Out-Null
            "Shutdown initiated on $VM`n"
            Sleep $timer
        }
    }

    if ($arg1 -eq "Shut down") 
    {
        WaitforVMoff -VMList $selectarray -Interval $waittime -Timeout $maxwait
    
    } elseif ($arg1 -eq "Restart") {

        WaitforVMreboot -VMList $selectarray -Interval $waittime -Timeout $maxwait
    }
    $selectarray.clear()
}



multivmoscreation -Gen $generation -Total $totalvms `
                  -RS5baselocation $RS5Template -RS5testVmName $RS5testVmName `
                  -W19H1baselocation $W19H1Template -W19H1testVmName $W19H1testVmName `
                  -W2019baselocation $W2019Template -W2019testVmName $W2019testVmName `
                  -EVDbaselocation $EVDTemplate -EVDtestVmName $EVDtestVmName `
                  -W2016baselocation $W2016Template -W2016testVmName $W2016testVmName `
                  -W20H1baselocation $W20H1Template -W20H1testVmName $W20H1testVmName

foreach ($vm in $vmnames)
{
    $vmarray.add($vm) | Out-Null
}

if ($sysprep -like 'y*') 
{
    WaitforSysprep $vmarray
}

vfmultiassignment -Guests $vmnames -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

robusttest $vmnames
Stop-Transcript

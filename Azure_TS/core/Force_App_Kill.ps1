﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string[]]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Is the template sysprepped (y/n)?")]
        [string]$sysprep,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] 
    [ValidateRange(1,8)]
        [int]$numofgpus,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$numofloops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$totalvms = 1 * $numofgpus
$testVMs = "$mydoc\testvms.txt"
$vmarray = new-object system.collections.arraylist
$curTime = (Get-Date).ToString("MMddHHmm")
$g_api = 'direct3d11', 'direct3d9', 'opengl'
$generation = 1

$RS5testVmName = "RS5-stresstest-"
$W19H1testVmName = "W19H1-stresstest-"
$EVDtestVmName = "EVD-stresstest-"
$W2019testVmName = "W2019-stresstest-"
$W2016testVmName = "W2016-stresstest-"
$W20H1testVmName = "W20H1-stresstest-"

"`nDo you have a local heaven copy? If not, please leave the following blank`n"
[AllowEmptyString()][string]$heavenlocation = Read-Host -Prompt "Enter your heaven local copy location (in quotes)"

"`nPlease enter Test VM credentials`n"
$cred = Get-Credential -Message VM -UserName AMD\

DefineTemplate $vmbaselocation

If ([string]::IsNullOrEmpty($heavenlocation))
{
    $heavenlocation = "\\loginvsi.amd.com\VirtDriver\ISV_Benchmarks\Heaven.zip"
} else {
    $extn = [IO.Path]::GetExtension($heavenlocation)
    if ($extn -ne ".zip" )
    {
        $unzipped = 1
    }
}

PrepRDP

multivmoscreation -Gen $generation -Total $totalvms `
                  -RS5baselocation $RS5Template -RS5testVmName $RS5testVmName `
                  -W19H1baselocation $W19H1Template -W19H1testVmName $W19H1testVmName `
                  -W2019baselocation $W2019Template -W2019testVmName $W2019testVmName `
                  -EVDbaselocation $EVDTemplate -EVDtestVmName $EVDtestVmName `
                  -W2016baselocation $W2016Template -W2016testVmName $W2016testVmName `
                  -W20H1baselocation $W20H1Template -W20H1testVmName $W20H1testVmName

foreach ($vm in $vmnames)
{
    $vmarray.add($vm) | Out-Null
}

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

AddInternalNetwork $vmarray

if ($sysprep -like 'y*') 
{
    WaitforSysprep $vmarray
}

vfmultiassignment -Guests $vmnames -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig

PoweronVMs $vmarray
Wait-VMs -timeout "600" -VMs $vmarray

Sleep 2

"`nPrepping VMs`n"
RPCsetup $vmarray $cred

"Transferring test items to VMs"
CopyHeaven $vmarray $cred $heavenlocation $unzipped

Sleep 30

"`nConnecting to VMs through RDP`n"
RDPConnect $vmarray $cred

$teststart = (Get-Date).TimeOfDay

#Test Loop
for ($loop = 1; $loop -le $numofloops; $loop++) 
{
    $randomapi = $g_api[(get-random -Maximum ([array]$g_api).count)]

    "`n`nStarting Heaven $randomapi`n"

    if (($sysprep -like 'y*') -and ($loop -eq 1))
    {
        Sleep 90
    } else
    {
        Sleep 25
    }


    if ($randomapi -like 'direct3d11') 
    {
        foreach ($heavenVM in $vmnames) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 direct3d11} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }

    } elseif ($randomapi -like 'direct3d9') {

        foreach ($heavenVM in $vmnames) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 direct3d9} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }

    } else {

        foreach ($heavenVM in $vmnames) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 opengl} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }
    }

    "`nStarting Unigine benchmark on all VMs`n"

    Sleep 10
    
    $whencheck = Get-Date

    foreach ($heavenVM in $vmnames) 
    {
        $checkheaven = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
        
        If (!$checkheaven)
        {
            "Benchmark failed to run in $heavenVM"
            Exit
        } 
        else
        {
            "Benchmark has started"
        }
    }

    "`nWaiting 60 seconds before process kill`n"
    sleep 60

    foreach ($heavenVM in $vmnames) 
    {
        $heavenkill = Invoke-Command -VMName $heavenVM -ScriptBlock {Stop-Process (Get-Process heaven) -Force} -credential $cred
        "`nProcess Kill initiated"

        "Checking activity..."
        $heavenactivity = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred

        If (!$heavenactivity)
        {
            "Benchmark successfully killed"
        } 
        else
        {
            "Benchmark still running. Attempt to force end process this time"
            $heavenkill = Invoke-Command -VMName $heavenVM -ScriptBlock {Stop-Process (Get-Process heaven) -Force} -credential $cred
            
            "Checking..."
            $heavenactivity = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
            If (!$heavenactivity)
            {
                "Benchmark successfully killed"
            } else
            {
                "Attempt to kill benchmark failed. Exiting here..."
                "Please check VM $heavenVM"
                Exit
            }
        } 
    }

    "`nChecking for TDR during app kill`n"
    #CheckTDR $whencheck $vmarray $cred
    CheckTDRonAllGuests $whencheck $vmarray $cred

    "`nChecking Guest Driver status...`n"
    CheckAllGuestDriver -VMs $vmarray -Credential $cred 

    "`nLoop $loop completed`n"
}

$testend = (Get-Date).TimeOfDay
"Force App Kill test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

Stop-Transcript
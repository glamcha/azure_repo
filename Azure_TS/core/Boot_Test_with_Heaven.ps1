﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Is the template sysprepped (y/n)?")]
        [string]$sysprep,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] 
    [ValidateRange(1,8)]
        [int]$numofgpus,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run this test")] 
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$numofloops,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

$totalvms = 1 * $numofgpus
$testVmName = "dynamicvf-"
$testVMs = "$mydoc\testvms.txt"
$vmarray = new-object system.collections.arraylist
$vmcheck = new-object system.collections.arraylist
$curTime = (Get-Date).ToString("MMddHHmm")
$g_api = 'direct3d11', 'direct3d9', 'opengl'
$generation = 1
$int_switch = Get-VMSwitch | where{$_.SwitchType -like '*internal*'}

"`nDo you have a local heaven copy? If not, please leave the following blank`n"
[AllowEmptyString()][string]$heavenlocation = Read-Host -Prompt "Enter your heaven local copy location (in quotes)"

If ([string]::IsNullOrEmpty($heavenlocation))
{
    $heavenlocation = "\\loginvsi.amd.com\VirtDriver\ISV_Benchmarks\Heaven.zip"
} else {

    $extn = [IO.Path]::GetExtension($heavenlocation)
    if ($extn -ne ".zip" )
    {
        $unzipped = 1
    }
}

"`nPlease enter Test VM credentials`n"
$cred = Get-Credential -Message VM -UserName AMD\


PrepRDP

testvmcreation -Gen $generation -Total $totalvms -VMName $testVmName -Template $vmbaselocation

$heavenVMs = Get-VM | Where-Object {$_.Name -match $testVmName}
foreach ($vm in $heavenVMs.Name)
{
    $vmarray.add($vm) | Out-Null
}

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

AddInternalNetwork $vmarray

if ($sysprep -like 'y*') 
{
    WaitforSysprep $vmarray
}

PoweronVMs $vmarray
Wait-VMs -timeout "600" -VMs $vmarray

"Transferring test items to VMs"
CopyHeaven $vmarray $cred $heavenlocation $unzipped

ShutdownVMs $vmarray

WaitforVMoff -VMList $vmarray -Interval "2" -Timeout "600"


$teststart = (Get-Date).TimeOfDay

#vfassignment -VMName $vmarray -VF $vfconfig -Adapter $numofgpus -CustomList 0 -Allocation "Performance" -VMResource $resourceconfig
vfmultiassignment -Guests $vmarray -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig

#Test Loop
for ($loop = 1; $loop -le $numofloops; $loop++) {

    "`n`nTesting $vfconfig VF config`n"

    sleep 2

    PoweronVMs $vmarray
    Wait-VMs -timeout "900" -VMs $vmarray

    Sleep 20

    "`nConnecting to VMs through RDP`n"
    RDPConnect $vmarray $cred

    if (($sysprep -like 'y*') -and ($loop -eq 1))
    {
        Sleep 90
    } else
    {
        Sleep 25
    }

    $randomapi = $g_api[(get-random -Maximum ([array]$g_api).count)]
    #$randomapi = 'direct3d11'

    "`nStarting Heaven $randomapi`n"

    if ($randomapi -like 'direct3d11') 
    {
        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 direct3d11} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }
    } elseif ($randomapi -like 'direct3d9') {
        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 direct3d9} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }
    } else {
        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $checkrdpid = Invoke-Command -VMName $heavenVM -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 opengl} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid
        }
    }

    "`nStarting Unigine benchmark on all VMs`n"

    Sleep 10
    
    $whencheck = Get-Date

    foreach ($heavenVM in $heavenVMs.Name) 
    {
        $waitforcompletion = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
        
        If (!$waitforcompletion)
        {
            "Benchmark failed to run in $heavenVM"
            Exit
        } 
        else
        {
            "Benchmark currently running"
            $vmcheck.add($heavenVM) | Out-Null
        }
    }

    "`nChecking for benchmark completion`n"

    $count = 1
    Do 
    {
        for ($x = 0; $x -lt $vmcheck.count; $x++)
        {
            $waitforcompletion = Invoke-Command -VMName $vmcheck[$x] -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
            if (!$waitforcompletion)
            {
                $targetvm = $vmcheck[$x]
                "$targetvm - Benchmark completed after $(10*$count) seconds"
                $vmcheck.RemoveAt($x)
                $x--
                ##TODO: Need to add result check for benchmark completion
            }
        }
        Sleep 10
        $count++
    } until (($vmcheck.count -eq 0) -or ($count -gt 90))

    If ($count -gt 90)
    {
        foreach ($failvm in $vmcheck)
        {
            "`nChecking for TDR during benchmark run`n"
            #CheckTDR $whencheck $vmarray $cred
            CheckTDRonAllGuests $whencheck $vmarray $cred

            "`n`nChecking Guest Driver status...`n"
            CheckAllGuestDriver -VMs $vmarray -Credential $cred  
           
            Write-Host "Benchmark timed out in $failvm - 15 minutes. Please check $failvm! Exiting here..." -BackgroundColor Red
            "Only $loop loops out of $numofloops completed"
            Exit
        } 
    }

    $vmcheck.Clear()

    "`nLoop $loop completed`n"
    "`nCopying results to \\loginvsi\logs\azure\Automated_Results\vfconfig`n"

    CopyHeavenResultstoserver $vmarray $cred
    ShutdownVMs $vmarray

    Sleep 2

    $rdpsession = get-process | Where-Object {$_.ProcessName -like 'mstsc'}
    Stop-Process $rdpsession -force -ErrorAction SilentlyContinue

    WaitforVMoff -VMList $vmarray -Interval "2" -Timeout "900"
    $vmarray.Clear()
}

$testend = (Get-Date).TimeOfDay
"Test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

Stop-Transcript
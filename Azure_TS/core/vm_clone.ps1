﻿
Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] [string]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Specify the test vm base name")] [string] $testVmName,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of vms to create")] [int]$totalvms
)

function testvmcreation()
{
    param([int] $totalvms, [string] $testVmName, [string] $vmbaselocation)

    $testvmdirectory = (Get-Item $vmbaselocation).PSDrive.Root
    $testvmlocation = $testvmdirectory + "\clones\"
    $vmswitch = (Get-VMSwitch | where-object{$_.SwitchType -like 'ext*'} | Select-Object -first 1).Name

    #$vhd_base = $vmbaselocation + "\Virtual Hard Disks\" + "*.vhdx"
    $ram = 1GB * 8
    $cpu = 4

    "`nCreating $totalvms vms`n"
    For ($vmcreated = 1; $vmcreated -le $totalvms; $vmcreated++)
    {
        $vmlabel = $testVmName + "-" + $vmcreated
        "`nCreating $vmlabel`n"

        New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation 1 -Version 9.0 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
        Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
        <#If ($generation -eq '2')
        {        
            Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
        }#>
        Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

        Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
        Copy-Item -Path $vmbaselocation -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
        Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
        <#If ($generation -eq '2')
        {
            $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		    Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive
        }#>

        "`n$vmlabel fully deployed`n"
    }
}

testvmcreation $totalvms $testVMname $vmbaselocation
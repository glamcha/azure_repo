﻿Param (
    [Parameter(Mandatory=$true, HelpMessage="Specify VM with driver template full path location")] 
    [ValidateScript({If ($_ -match "[a-zA-Z]+:\\.*\.vhd") {$True} else {Throw "$_ is not a valid image file"}})]
        [string]$vmbaselocation,
    [Parameter(Mandatory=$true, HelpMessage="Specify VF config")] 
    [ValidateSet("1", "2", "4", "8")]
        [int]$vfconfig,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of boards to test with")] 
    [ValidateRange(1,8)]
        [int]$numofgpus,
    [Parameter(Mandatory=$true, HelpMessage="Specify number of loops to run test")]
    [ValidateRange(1, [int32]::MaxValue)]
        [int32]$numofloops,
    [Parameter(Mandatory=$true, HelpMessage="Specify name of log folder on loginvsi")] 
        [string]$logfoldername,
    [Parameter(Mandatory=$true, HelpMessage="Use Azure default setting or 8GB setting")] 
        [string]$resourceconfig
)

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

[string[]]$apitest = (Read-Host -Prompt "Choose from ('dx11' 'dx9' 'ogl') (separate them by comma)").split(',') | % {$_.trim()}

$totalvms = $vfconfig * $numofgpus
$testVmName = "heaventest-"
$testVMs = "$mydoc\testvms.txt"
$logfile = "$mydoc\heaven_" + $time +"_log.txt"
$vmarray = new-object system.collections.arraylist
$vmcheck = new-object system.collections.arraylist
$curTime = (Get-Date).ToString("MMddyyyyHHmm")
$generation = 1
$int_switch = Get-VMSwitch | where{$_.SwitchType -like '*internal*'}


PrepRDP

"`nPlease enter VM credentials`n"
$cred = Get-Credential -Message VM -UserName AMD\

testvmcreation -Gen $generation -Total $totalvms -VMName $testVmName -Template $vmbaselocation

$heavenVMs = Get-VM | Where-Object {$_.Name -match $testvmName}
foreach ($vm in $heavenVMs.Name)
{
    $vmarray.add($vm) | Out-Null
}

vfmultiassignment -Guests $vmarray -VF $vfconfig -Allocation "Performance" -VMResource $resourceconfig
#vfassignment -VMName $vmarray -VF $vfconfig -Adapter $numofgpus -CustomList 0 -Allocation "Performance" -VMResource $resourceconfig

sleep 2

AddInternalNetwork $vmarray

if ($rome64flag -eq 1)
{
    cpugroups $vmarray
}

PoweronVMs $vmarray
Wait-VMs -timeout "600" -VMs $vmarray

"Transferring test items to VMs"
CopyHeaven $vmarray $cred $heavenlocation 0

Sleep 3

"`nConnecting to VMs through RDP"
RDPConnect $vmarray $cred

Sleep 30

$checkrdpid = Invoke-Command -VMName heaventest-1 -ScriptBlock {(Get-Process -IncludeUserName | Where {$_.ProcessName -like '*rdp*'}).SessionId} -credential $cred
$apitest = $apitest.Replace('dx11','direct3d11')
$apitest = $apitest.Replace('dx9','direct3d9')
$apitest = $apitest.Replace('ogl','opengl')

$teststart = (Get-Date).TimeOfDay

for ($loop = 1; $loop -le $numofloops; $loop++) 
{
    "`n`nLoop $loop`n"
    for ($apirun = 0; $apirun -lt $apitest.count; $apirun++)
    {
        Logging "`nLaunching Heaven $($apitest[$apirun]) benchmark`n" $logfile

        foreach ($heavenVM in $heavenVMs.Name) 
        {
            Invoke-Command -VMName $heavenVM -ScriptBlock {param($checkrdpid, $apitest, $apirun) PsExec64 -s -i $checkrdpid -d -accepteula Powershell C:\Heaven\heaven_run.ps1 1 $apitest[$apirun]} -Credential $cred -AsJob:$true -ArgumentList $checkrdpid,$apitest,$apirun
        }

        sleep 10

        $whencheck = Get-Date

        foreach ($heavenVM in $heavenVMs.Name) 
        {
            $waitforcompletion = Invoke-Command -VMName $heavenVM -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
        
            If (!$waitforcompletion)
            {
                "Benchmark failed to run in $heavenVM"
                Exit
            } 
            else
            {
                "Benchmark currently running"
                $vmcheck.add($heavenVM) | Out-Null
            }
        }

        "`nChecking for benchmark completion`n"

        $count = 1
        Do 
        {
            for ($x = 0; $x -lt $vmcheck.count; $x++)
            {
                $waitforcompletion = Invoke-Command -VMName $vmcheck[$x] -ScriptBlock {Get-Process heaven -ErrorAction SilentlyContinue} -credential $cred
                if (!$waitforcompletion)
                {
                    $targetvm = $vmcheck[$x]
                    "$targetvm - Benchmark completed after $(10*$count) seconds"
                    $vmcheck.RemoveAt($x)
                    $x--
                ##TODO: Need to add result check for benchmark completion
                }
            }

            Sleep 10
            $count++

        } until (($vmcheck.count -eq 0) -or ($count -gt 90))


        If ($count -gt 90)
        {
            foreach ($failvm in $vmcheck)
            {
                "`n`nChecking Guest Driver status...`n"
                CheckAllGuestDriver -VMs $vmarray -Credential $cred
            
                "`nChecking for TDR during benchmark run`n"
                #CheckTDR $whencheck $vmarray $cred
                CheckTDRonAllGuests $whencheck $vmarray $cred

                Write-Host "Benchmark timed out in $failvm - 15 minutes. Please check $failvm! Exiting here..." -BackgroundColor Red
                Exit
            } 
        }
        $vmcheck.Clear()
    }
}

$testend = (Get-Date).TimeOfDay
"Heaven test completed in $([math]::Round(($testend - $teststart).TotalMinutes,3)) minutes"

"`nCopying Results to \\loginvsi\Logs\Azure\Automated_Results\HeavenAPI\$logfoldername`n"

foreach ($heavenVM in $heavenVMs.Name) 
{
    Invoke-Command -VMName $heavenVM -ScriptBlock {param($heavenVM, $logfoldername)
        $ip = (Get-NetIPAddress -AddressFamily IPv4 | where {($_.InterfaceAlias -notmatch 'Loopback') -and ($_.PrefixOrigin -like 'Dhcp')}).IPAddress | select -first 1
        new-item -itemtype directory -force -path \\loginvsi\logs\azure\Automated_Results\HeavenAPI\$logfoldername\$heavenVM
        Copy-Item -Path C:\Heaven\api_run.csv -Destination \\loginvsi\Logs\Azure\Automated_Results\HeavenAPI\$logfoldername\$heavenVM\heaven_api.csv -Force
        Rename-Item -Path C:\Heaven\api_run.csv -NewName C:\Heaven\results_old.csv -ErrorAction SilentlyContinue -Force 
    } -credential $cred -ArgumentList $heavenVM,$logfoldername
}

Stop-Transcript
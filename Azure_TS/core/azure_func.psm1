﻿function hostcheck()
{
    $servercpu =  (Get-WmiObject win32_processor).Name
    $servercores =(Get-WmiObject win32_processor | Select-Object -property name, number*).NumberofEnabledCore

    $sockets = $servercores.count

    if($servercores.gettype().isarray)
    {
        $cores = $servercores[0]
    } else
    {
        $cores = $servercores[0]
    }

    if (($cores -eq '64') -and ($servercpu -match "EPYC 7\w[0-9]2"))
    {
        "ROME server with $sockets sockets - $cores cores each" | out-file $env:TEMP\cpumodel.txt
        $global:rome64flag = 1
    } else
    {
        "Non-ROME server with $sockets sockets - $cores cores each" | out-file $env:TEMP\cpumodel.txt
        $global:rome64flag = 0
    }

}

function envcheck()
{

    $rootproc = bcdedit /enum `{current`} | select-string -pattern "(?<=hypervisorrootproc      )\d+" | % {$_.Matches.Value}
    $rootprocnumanodes = bcdedit /enum `{current`} | select-string -pattern "(?<=hypervisorrootprocnumanodes)\d+"
    
    $nodesbcdsettings = $rootprocnumanodes.ToString() -replace "[a-zA-Z]"

    if (!((Get-VMHost).NumaSpanningEnabled))
    {
        Set-VMHost -NumaSpanningEnabled $true
        "Numa Spanning Enabled`n"
    }

    if ($rootproc -ne 64)
    {
        bcdedit.exe /set hypervisorrootproc 64
    }

    if ($nodesbcdsettings -ne '0,1,2,3,16,17,18,19')
    {
        bcdedit.exe /set hypervisorrootprocnumanodes "0,1,2,3,16,17,18,19"
        "Please reboot the host now, then re-run the test"
        Exit
    }
}


function cpugroups()
{
    param(
        [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs
    )
    
    if (!(Test-Path $root\cpugroups\CpuGroups.exe))
    {
        "Error: Missing CPUGROUPS executable in script folder"
        Exit
    }

    $cpugroupexe = "$root\cpugroups\CpuGroups.exe"

    Start-Process -FilePath $cpugroupexe -ArgumentList "CreateGroup", "/GroupId:36AB08CB-3A76-4B38-992E-000000000001", "/GroupAffinity:64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255" -NoNewWindow -Wait 


    $checkpath = Test-Path $testVMs


    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testarray = Get-content $testVMs
    } elseif ( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testarray = $testVMs.Name
    } else
    {
        [System.Collections.ArrayList] $testarray = $testVMs.Clone()
    }

    Foreach ($vm in $testarray)
    {
        Start-Process -FilePath $cpugroupexe -ArgumentList "SetVmGroup", "/VmName:$vm", "/GroupId:36AB08CB-3A76-4B38-992E-000000000001" -NoNewWindow -Wait 
        #CpuGroups.exe SetVmGroup /VmName:<VM_NAME> /GroupId:36AB08CB-3A76-4B38-992E-000000000001
    }

    Invoke-Expression "$cpugroupexe getvmgroup" > VMGroup.txt
    $checkvmgroup = Invoke-Expression "$cpugroupexe getgroupvms" | Select-String "36AB08CB-3A76-4B38-992E-000000000001"

    If (!($checkvmgroup))
    {
        "VMs not assigned to selected CPU group. Please check VMGroup.txt! Exiting..."
        Exit
    }

    $testarray.clear()

}

function testvmcreation()
{
    param(
        [Alias('Gen')]
        [ValidateRange(1,2)]
            [int] $generation,
        [Alias('Total')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $totalvms, 
        [Alias('VMName')]
        [ValidateNotNullOrEmpty()]
            [string] $testVmName, 
        [Alias('Template')]
        [ValidateNotNullOrEmpty()]
            [string] $vmbaselocation,
        [Alias('W2012VMName')]
            [AllowEmptyString()] [string] $secondtestVmName,
        [Alias('W2012Template')]
            [AllowEmptyString()] [string]$vmsecondbaselocation
    )

    $cpu = 4
    $ram = 1GB * 8
    $vmswitch = (Get-VMSwitch | where-object{$_.SwitchType -like 'ext*'} | Select-Object -first 1).Name
    $testvmdirectory = (Get-Item $vmbaselocation).PSDrive.Root
    $testvmlocation = $testvmdirectory + "\testvms\"
    

    If ([string]::IsNullOrEmpty($vmsecondbaselocation)) 
    {
        "`nCreating $totalvms VMs`n"

        For ($vmcreated = 1; $vmcreated -le $totalvms; $vmcreated++)
        {
            $vmlabel = $testVmName + $vmcreated
            "`nCreating $vmlabel`n"

            New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation $generation -Version 9.0 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
            Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
            If ($generation -eq '2')
            {        
                Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
            }
            Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

            Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
            Copy-Item -Path $vmbaselocation -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            If ($generation -eq '2')
            {
                $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		        Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive	
            }
            "`n$vmlabel fully deployed`n"
            sleep 1
        }

    } else {

        $testsecondvmdirectory = (Get-Item $vmsecondbaselocation).PSDrive.Root

        $vmswitch = (Get-VMSwitch | where-object{$_.SwitchType -like 'ext*'} | Select-Object -first 1).Name

        "`nCreating vms from 1st template`n"

        For ($vmcreated = 1; $vmcreated -le [int]$($totalvms/2); $vmcreated++)
        {
            $vmlabel = $testVmName + "OS1-" + $vmcreated
            "`nCreating $vmlabel`n"

            New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation $generation -Version 9.0 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
            Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
            If ($generation -eq '2')
            {        
                Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
            }
            Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

            Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
            Copy-Item -Path $vmbaselocation -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            If ($generation -eq '2')
            {
                $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		        Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive	
            }
            "`n$vmlabel fully deployed`n"
            sleep 1
        }

        $vmslefttodeploy = $totalvms - [int]$($totalvms/2)
        "`nCreating $vmslefttodeploy vms from 2nd template`n"

        For ($vmcreated = 1; $vmcreated -le $vmslefttodeploy; $vmcreated++)
        {
            $vmlabel = $secondtestVmName + "OS2-" + $vmcreated
            "`nCreating $vmlabel`n"

            New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation 2 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
            Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
            If ($generation -eq '2')
            {
                Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
            }
            Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

            Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
            Copy-Item -Path $vmsecondbaselocation -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            If ($generation -eq '2')
            {
                $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		        Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive	
            }
            "`n$vmlabel fully deployed`n"
            sleep 1
        }
    }
}

function vfassignment()
{
    param(
        [Alias('VMName')]
        [ValidateNotNullOrEmpty()]
            [string[]] $test_vm, 
        [Alias('VF')]
        [ValidateSet("1", "2", "4", "8")]
            [int] $vfconfig, 
        [Alias('Adapter')]
        [ValidateRange(1,8)]
            [int] $numofgpus,
        [Alias('CustomList')]
            [AllowEmptyString()] [int] $list_option,
        [Alias('Allocation')]
            [AllowEmptyString()] [string] $performance,
        [Alias('VMResource')]
            [string] $resourceconfig
    )
    
    $testvmsarray = New-Object System.Collections.ArrayList
    $adapternum = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first $numofgpus

    if ($resourceconfig -like 'A*')
    {

        if ($vfconfig -eq 1) 
        {
            [int]$cpu = 16
            [int64]$ram = 1GB * 112

        } elseif ($vfconfig -eq 2) {

            [int]$cpu = 8
            [int64]$ram = 1GB * 56

        } elseif ($vfconfig -eq 4) {

            [int]$cpu = 4
            [int64]$ram = 1GB * 28

        } elseif ($vfconfig -eq 8) {

            [int]$cpu = 2
            [int64]$ram = 1GB * 14

        } else {

            "`nInvalid VF configuration`n"
            exit
        }
    }
    else
    {
        if ($vfconfig -eq 1) 
        {
            [int]$cpu = 16
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 2) {

            [int]$cpu = 8
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 4) {

            [int]$cpu = 4
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 8) {

            [int]$cpu = 2
            [int64]$ram = 1GB * 8

        } else {

            "`nInvalid VF configuration`n"
            exit
        }
    }

    CheckRunningVMs

    sleep 4

    if ($list_option -eq 1) 
    {
        [System.Collections.Hashtable]$customvms = Import-PowerShellDataFile "$root\customvms.psd1"
        $vms_in_testing = $customvms.custom_vm_list 

    } else {
        $vms_in_testing = Get-VM | Where-Object {$_.Name -match $test_vm}
    }

    foreach ($singlevm in $vms_in_testing.Name) 
    {
        $listvm = $testvmsarray.add($singlevm)
    }

    foreach ($vm_in_testing in $vms_in_testing.Name) 
    {
        $gpuCheck = Get-VMGpuPartitionAdapter -VMName $vm_in_testing -ErrorAction SilentlyContinue
        if ($resourceconfig -notlike 'C*'){
            Set-VM -Name $vm_in_testing -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory -MemoryStartupBytes $ram
        }

        if ($gpuCheck) 
        {
        "`nRemoving adapter`n"
        Remove-VMGpuPartitionAdapter -VMName $vm_in_testing
        }
    }
			
    foreach ($gpu in $adapternum) 
    {
        $gpuid = $gpu.Name | format-table -HideTableHeaders | Out-String
	    Set-VMPartitionableGpu -Name $gpuid.trim() -PartitionCount $vfconfig -ErrorAction Stop				
        #$vms_in_testing = Get-VM | Where-Object {$_.Name -match $test_vm}
    }

    $updatedadapters = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first $numofgpus

    if (($performance -eq '0') -or ($performance -eq $null) -or ($performance -like 'Density'))
    {
        foreach ($gpu in $updatedadapters) 
        {
            for ($i = 0; $i -lt $testvmsarray.count; $i++) 
            {
                $testvmname = $testvmsarray[$i]

                "`nAttaching GPU$i VF-$vfconfig to $testvmname`n"
                Set-VM -GuestControlledCacheTypes 1 -LowMemoryMappedIoSpace 1Gb -HighMemoryMappedIoSpace 32Gb -VMName $testvmname
                Add-VMGpuPartitionAdapter -MinPartitionVRAM $gpu.MinPartitionVRAM -MaxPartitionVRAM  $gpu.MaxPartitionVRAM -OptimalPartitionVRAM  $gpu.OptimalPartitionVRAM `
                                      -MinPartitionEncode $gpu.MinPartitionEncode -MaxPartitionEncode $gpu.MaxPartitionEncode -OptimalPartitionEncode $gpu.OptimalPartitionEncode `
                                      -MinPartitionDecode $gpu.MinPartitionDecode -MaxPartitionDecode $gpu.MaxPartitionDecode -OptimalPartitionDecode $gpu.OptimalPartitionDecode `
                                      -MinPartitionCompute $gpu.MinPartitionCompute -MaxPartitionCompute $gpu.MaxPartitionCompute -OptimalPartitionCompute $gpu.OptimalPartitionCompute `
                                      -VMName $testvmname -ErrorAction Stop
                Get-VMGpuPartitionAdapter -VMName $testvmname >> gpu_vm.log
			    $testvmsarray.RemoveAt($i)
                $i--					
            }
        }
    } elseif (($performance -eq '1') -or ($performance -like 'Perf')) 
    {
        for ($i = 0; $i -lt $testvmsarray.count; $i++) 
        {
            $testvmname = $testvmsarray[$i]

            if ($i -ge $numofgpus)
            {
                $i = $i % $numofgpus
                $gpu = $updatedadapters[$i]
            } else
            {
                $gpu = $updatedadapters[$i]
            }

            "`nAttaching GPU$i VF-$vfconfig to $testvmname`n"
            Set-VM -GuestControlledCacheTypes 1 -LowMemoryMappedIoSpace 1Gb -HighMemoryMappedIoSpace 32Gb -VMName $testvmname
            Add-VMGpuPartitionAdapter -MinPartitionVRAM $gpu.MinPartitionVRAM -MaxPartitionVRAM  $gpu.MaxPartitionVRAM -OptimalPartitionVRAM  $gpu.OptimalPartitionVRAM -MinPartitionEncode $gpu.MinPartitionEncode -MaxPartitionEncode $gpu.MaxPartitionEncode -OptimalPartitionEncode $gpu.OptimalPartitionEncode -MinPartitionDecode $gpu.MinPartitionDecode -MaxPartitionDecode $gpu.MaxPartitionDecode -OptimalPartitionDecode $gpu.OptimalPartitionDecode -MinPartitionCompute $gpu.MinPartitionCompute -MaxPartitionCompute $gpu.MaxPartitionCompute -OptimalPartitionCompute $gpu.OptimalPartitionCompute -VMName $testvmname -ErrorAction Stop
            Get-VMGpuPartitionAdapter -VMName $testvmname >> gpu_vm.log
			$testvmsarray.RemoveAt($i)
            $i--					
        }        
    }
}

function multivmoscreation()
{
    param(
        [Alias('Gen')]
        [ValidateRange(1,2)]
            [int] $generation,
        [Alias('Total')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $totalvms, 
        [Alias('RS5VMName')]
            [AllowEmptyString()] [string] $RS5testVmName, 
        [Alias('RS5Template')]
            [AllowEmptyString()] [string] $RS5baselocation,
        [Alias('W19H1VMName')]
            [AllowEmptyString()] [string] $W19H1testVmName,
        [Alias('W19H1Template')]
            [AllowEmptyString()] [string]$W19H1baselocation,
        [Alias('W2016VMName')]
            [AllowEmptyString()] [string] $W2016testVmName,
        [Alias('W2016Template')]
            [AllowEmptyString()] [string]$W2016baselocation,
        [Alias('W2019VMName')]
            [AllowEmptyString()] [string] $W2019testVmName,
        [Alias('W2019Template')]
            [AllowEmptyString()] [string]$W2019baselocation,
        [Alias('EVDVMName')]
            [AllowEmptyString()] [string] $EVDtestVmName,
        [Alias('EVDTemplate')]
            [AllowEmptyString()] [string]$EVDbaselocation,
        [Alias('W20H1VMName')]
            [AllowEmptyString()] [string] $W20H1testVmName,
        [Alias('W20H1Template')]
            [AllowEmptyString()] [string]$W20H1baselocation
    )

    $cpu = 4
    $ram = 1GB * 8
    $vmswitch = (Get-VMSwitch | where-object{$_.SwitchType -like 'ext*'} | Select-Object -first 1).Name
    #$testvmdirectory = (Get-Item $vmbaselocation[0]).PSDrive.Root
    #$testvmlocation = $testvmdirectory + "\testvms\"
    $templatearray = new-object system.collections.arraylist
    $osarray = new-object system.collections.arraylist
    $global:vmnames = new-object system.collections.arraylist

    If (!([string]::IsNullOrEmpty($RS5baselocation)))
    {
        $templatearray.Add($RS5baselocation) | Out-Null
        $osarray.Add($RS5testVmName) | Out-Null
    }

    If (!([string]::IsNullOrEmpty($W19H1baselocation)))
    {
        $templatearray.Add($W19H1baselocation) | Out-Null
        $osarray.Add($W19H1testVmName) | Out-Null
    }


    If (!([string]::IsNullOrEmpty($W2016baselocation)))
    {
        $templatearray.Add($W2016baselocation) | Out-Null
        $osarray.Add($W2016testVmName) | Out-Null
    }


    If (!([string]::IsNullOrEmpty($W2019baselocation)))
    {
        $templatearray.Add($W2019baselocation) | Out-Null
        $osarray.Add($W2019testVmName) | Out-Null
    }


    If (!([string]::IsNullOrEmpty($EVDbaselocation)))
    {
        $templatearray.Add($EVDbaselocation) | Out-Null
        $osarray.Add($EVDtestVmName) | Out-Null
    }

    If (!([string]::IsNullOrEmpty($W20H1baselocation)))
    {
        $templatearray.Add($W20H1baselocation) | Out-Null
        $osarray.Add($W20H1testVmName) | Out-Null
    }

    If (([string]::IsNullOrEmpty($EVDbaselocation)) -and ([string]::IsNullOrEmpty($W2016baselocation)) -and ([string]::IsNullOrEmpty($W2019baselocation)) -and ([string]::IsNullOrEmpty($W19H1baselocation)) -and ([string]::IsNullOrEmpty($RS5baselocation))-and ([string]::IsNullOrEmpty($W20H1baselocation)))
    {
        Write-Error "No Template selected"
        Exit
    }

    "`nNumber of OSes to test on: $templatearray.Count`n"

    If ($templatearray.Count -le $totalvms)
    {
        "`nTemplates chosen:"
        $templatearray | foreach-object {"$_`n"}
        "`nNumber of VMs to create: $totalvms`n"

        $vmoscount = [math]::floor($totalvms/$templatearray.Count)

    } else {
        
        "`nNumber of OSes selected exceeds number of VMs to create`n"
        $ignoreoscountwarning = Read-Host -Prompt "Some of the OSes won't be tested. Proceed (y/n)"
        
        If ($ignoreoscountwarning -eq 'y')
        {
            "`nTemplates chosen:"
            $templatearray | foreach-object {"$_`n"}
        } else {
            "`nPlease increase number of VF or GPU or decrease number of different types of OSes, to test with`n"
            Exit
        }
    }


    For ($x = 0; $x -lt $templatearray.Count; $x++)
    {
        "`nDeploying $vmoscount VM with " + $templatearray[$x]

        For ($vmtocreate = 1; $vmtocreate -le $vmoscount; $vmtocreate++)
        {
            $vmlabel = $osarray[$x] + $vmtocreate
            "`nCreating $vmlabel`n"

            New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation $generation -Version 9.0 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
            Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
            If ($generation -eq '2')
            {        
                Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
            }
            Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

            Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
            Copy-Item -Path $templatearray[$x] -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
            If ($generation -eq '2')
            {
                $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		        Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive	
            }
            "`n$vmlabel fully deployed`n" 
            
            $vmnames.Add($vmlabel) | Out-Null   
            sleep 2
        }   
    }

    $vmlefttodeploy = $totalvms - ($templatearray.Count * $vmoscount)

    For ($y = 0; $y -lt $vmlefttodeploy; $y++)
    {
        $randomos = (Get-Random -Maximum ([array]$osarray).Count)
        $vmlabel = $osarray[$randomos] + "R" + $y
        "`nCreating $vmlabel`n"

        New-VM -Name $vmlabel -Path $testvmlocation -NoVHD -Generation $generation -Version 9.0 -MemoryStartupBytes $ram -SwitchName $vmswitch | Out-Null
        Set-VM -Name $vmlabel -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory
        If ($generation -eq '2')
        {        
            Set-VMFirmware -VMName $vmlabel -EnableSecureBoot Off
        }
        Set-VMProcessor -VMName $vmlabel -CompatibilityForMigrationEnabled 0

        Enable-VMIntegrationService -VMName $vmlabel -Name 'Guest Service Interface'
        Copy-Item -Path $templatearray[$randomos] -Destination $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
        Add-VMHardDiskDrive -VMName $vmlabel -Path $($testvmlocation + $vmlabel + "\" + $vmlabel + ".vhdx")
        If ($generation -eq '2')
        {
            $bootfromDrive = Get-VMFirmware -VMName $vmlabel | Select-Object -ExpandProperty BootOrder | Where-Object {$_.BootType -like "Drive"}							
		    Set-VMFirmware -VMName $vmlabel -BootOrder $bootfromDrive	
        }
        "`n$vmlabel fully deployed`n" 

        $vmnames.Add($vmlabel) | Out-Null
        sleep 2
    }

    $templatearray.Clear()
    $osarray.Clear()
}


function vfmultiassignment ()
{
    param(
        [Alias('Guests')]
        [ValidateNotNullOrEmpty()] 
            [System.Collections.ArrayList]$GuestVMs,
        [Alias('VF')]
        [ValidateSet("1", "2", "4", "8")]
            [int] $vfconfig, 
        [Alias('Allocation')]
            [string] $performance,
        [Alias('VMResource')]
            [string] $resourceconfig
    )
    
    $testvmsarray = New-Object System.Collections.ArrayList
    #[System.Collections.ArrayList] $listVMs = Get-content $GuestVMs 
    [int] $availablegpus = (Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | measure).Count
    $adapter = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first $availablegpus
    [int] $totalvfs = $availablegpus * $vfconfig

    If ($listVMs.Count -gt $totalvfs)
    {
        Write-Error "`nNumber of VMs exceeds total number of VFs available`n" -Category QuotaExceeded -RecommendedAction "Please make sure there are enough VFs to accommodate your test VMs"
        Exit
    }


    if ($resourceconfig -like 'A*')
    {
        if ($vfconfig -eq 1) 
        {
            [int]$cpu = 32
            [int64]$ram = 1GB * 112

        } elseif ($vfconfig -eq 2) {

            [int]$cpu = 16
            [int64]$ram = 1GB * 56

        } elseif ($vfconfig -eq 4) {

            [int]$cpu = 8
            [int64]$ram = 1GB * 28

        } elseif ($vfconfig -eq 8) {

            [int]$cpu = 4
            [int64]$ram = 1GB * 14

        } else {

            "`nInvalid VF configuration`n"
            exit
        }
    }
    else
    {
        if ($vfconfig -eq 1) 
        {
            [int]$cpu = 32
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 2) {

            [int]$cpu = 16
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 4) {

            [int]$cpu = 8
            [int64]$ram = 1GB * 8

        } elseif ($vfconfig -eq 8) {

            [int]$cpu = 4
            [int64]$ram = 1GB * 8

        } else {

            "`nInvalid VF configuration`n"
            exit
        }
    }

    CheckRunningVMs

    sleep 4

    foreach ($singlevm in $GuestVMs) 
    {
        $testvmsarray.add($singlevm) | Out-Null
    }

    foreach ($vm in $GuestVMs) 
    {
        $gpuCheck = Get-VMGpuPartitionAdapter -VMName $vm -ErrorAction SilentlyContinue

        if ($resourceconfig -notlike 'C*'){
            Set-VM -Name $vm -ProcessorCount $cpu -AutomaticStartAction Nothing -StaticMemory -MemoryStartupBytes $ram
        }

        if ($gpuCheck) 
        {
        "`nRemoving adapter`n"
        Remove-VMGpuPartitionAdapter -VMName $vm
        }
    }
			
    foreach ($gpu in $adapter) 
    {
        $gpuid = $gpu.Name | format-table -HideTableHeaders | Out-String
	    Set-VMPartitionableGpu -Name $gpuid.trim() -PartitionCount $vfconfig -ErrorAction Stop				
    }

    $updatedadapters = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first $availablegpus

    if (($performance -eq '0') -or ($performance -eq $null) -or ($performance -eq 'density'))
    {
        foreach ($gpu in $updatedadapters) 
        {
            for ($i = 0; $i -lt $testvmsarray.count; $i++) 
            {
                $testvmname = $testvmsarray[$i]

                "`nAttaching GPU$i VF-$vfconfig to $testvmname`n"
                Set-VM -GuestControlledCacheTypes 1 -LowMemoryMappedIoSpace 1Gb -HighMemoryMappedIoSpace 32Gb -VMName $testvmname
                Add-VMGpuPartitionAdapter -MinPartitionVRAM $gpu.MinPartitionVRAM -MaxPartitionVRAM  $gpu.MaxPartitionVRAM -OptimalPartitionVRAM  $gpu.OptimalPartitionVRAM `
                                      -MinPartitionEncode $gpu.MinPartitionEncode -MaxPartitionEncode $gpu.MaxPartitionEncode -OptimalPartitionEncode $gpu.OptimalPartitionEncode `
                                      -MinPartitionDecode $gpu.MinPartitionDecode -MaxPartitionDecode $gpu.MaxPartitionDecode -OptimalPartitionDecode $gpu.OptimalPartitionDecode `
                                      -MinPartitionCompute $gpu.MinPartitionCompute -MaxPartitionCompute $gpu.MaxPartitionCompute -OptimalPartitionCompute $gpu.OptimalPartitionCompute `
                                      -VMName $testvmname -ErrorAction Stop
                Get-VMGpuPartitionAdapter -VMName $testvmname >> gpu_vm.log
			    $testvmsarray.RemoveAt($i)
                $i--					
            }
        }
    } elseif (($performance -eq '1') -or ($performance -eq 'Performance'))
    {
        for ($i = 0; $i -lt $testvmsarray.count; $i++) 
        {
            $testvmname = $testvmsarray[$i]

            if ($i -ge $availablegpus)
            {
                $i = $i % $availablegpus
                $gpu = $updatedadapters[$i]
            } else
            {
                $gpu = $updatedadapters[$i]
            }

            "`nAttaching GPU$i VF-$vfconfig to $testvmname`n"
            Set-VM -GuestControlledCacheTypes 1 -LowMemoryMappedIoSpace 1Gb -HighMemoryMappedIoSpace 32Gb -VMName $testvmname
            Add-VMGpuPartitionAdapter -MinPartitionVRAM $gpu.MinPartitionVRAM -MaxPartitionVRAM  $gpu.MaxPartitionVRAM -OptimalPartitionVRAM  $gpu.OptimalPartitionVRAM -MinPartitionEncode $gpu.MinPartitionEncode -MaxPartitionEncode $gpu.MaxPartitionEncode -OptimalPartitionEncode $gpu.OptimalPartitionEncode -MinPartitionDecode $gpu.MinPartitionDecode -MaxPartitionDecode $gpu.MaxPartitionDecode -OptimalPartitionDecode $gpu.OptimalPartitionDecode -MinPartitionCompute $gpu.MinPartitionCompute -MaxPartitionCompute $gpu.MaxPartitionCompute -OptimalPartitionCompute $gpu.OptimalPartitionCompute -VMName $testvmname -ErrorAction Stop
            Get-VMGpuPartitionAdapter -VMName $testvmname >> gpu_vm.log
			$testvmsarray.RemoveAt($i)
            $i--					
        }        
    }
}


function DefineTemplate([System.Collections.ArrayList]$vmbaselocation)
{
    [System.Collections.ArrayList] $oslist ="RS5", "W2019", "W2016", "19H1", "EVD", "20H1"
    $global:testvmdirectory = (Get-Item $vmbaselocation[0]).PSDrive.Root
    $global:testvmlocation = $testvmdirectory + "testvms\"
    
    For ($i = 0; $i -lt $vmbaselocation.Count; $i++)
    {
        If ($vmbaselocation[$i] -match 'RS5')
        {
            $global:RS5Template = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("RS5")
            $i--
        } elseif ($vmbaselocation[$i] -match '19H1')
        {
            $global:W19H1Template = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("19H1")
            $i--
        } elseif ($vmbaselocation[$i] -match 'EVD')
        {
            $global:EVDTemplate = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("EVD")
            $i--
        } elseif ($vmbaselocation[$i] -match '2019')
        {
            $global:W2019Template = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("W2019")
            $i--
        } elseif ($vmbaselocation[$i] -match '2016')
        {
            $global:W2016Template = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("W2016")
            $i--
        } elseif ($vmbaselocation[$i] -match '20H1')
        {
            $global:W20H1Template = $vmbaselocation[$i]
            $vmbaselocation.RemoveAt($i)
            $oslist.Remove("20H1")
            $i--
        }
    }

    If ($vmbaselocation.Count -gt 0)
    {
        Do
        {
            for ($j = 0; $j -lt $vmbaselocation.count; $j++)
            {
                $x = Read-Host -Prompt "Define this template $vmbaselocation[$j]: [$oslist]"
            
                If ($x -match "RS")
                {
                    $global:RS5Template = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("RS")
                    $j--   
                } elseif ($x -match "19H1")
                {
                    $global:W19H1Template = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("19H1")
                    $j--   
                } elseif ($x -match "2019")
                {
                    $global:W2019Template = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("W2019")
                    $j--   
                } elseif ($x -match "2016")
                {
                    $global:W2016Template = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("W2016")
                    $j--   
                } elseif ($x -match "EVD")
                {
                    $global:EVDTemplate = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("EVD")
                    $j--   
                } elseif ($x -match "20H1")
                {
                    $global:W20H1Template = $vmbaselocation[$j]
                    $vmbaselocation.RemoveAt($j)
                    $oslist.Remove("20H1")
                    $j--   
                } else
                {
                    "Invalid Definition"
                    Exit
                }
            }

        } while ($vmbaselocation.count -gt 0)
    }
}



Function Logging ([String[]] $logs, $logfile)
{
   $timestamp = $([DateTime]::Now).ToString()
 
   if ($logs -match "failed")
   {
      Write-Host "$logs" -BackgroundColor Red
      Add-Content $logfile "$timestamp $logs"
   }
   else
   {
      Write-Host "$logs"
      Add-Content $logfile "$timestamp $logs"
   }
}

Function checktemploc ()
{
    Write-Host "`nLooking for Templates in non-boot drives...`n"

    $OSdrive = (Get-WmiObject Win32_OperatingSystem).SystemDrive
    [array] $drives = (Get-WmiObject win32_logicaldisk -filter "Drivetype = 3" | Select-Object deviceid | where-object{$_.deviceid -ne $OSdrive})

    ForEach($drive in $drives.deviceid) 
    { 
        $path = $drive + "\Program*"
        if (Test-path $path) 
        {
            $drives = $drives | ? {$_.deviceid -ne $drive}
        }
    }

    ForEach($drive in $drives) 
    { 
        (Get-ChildItem ($drive.deviceid + "\") -recurse -exclude D:\ -ErrorAction SilentlyContinue | where-object {($_.Name -like '*base*.vhdx') -or ($_.Name -like '*temp*.vhdx') -or ($_.Name -like '*RC*.vhdx') -or ($_.Name -like '*Alpha*.vhdx') -or ($_.Name -like '*sysprep*.vhdx')}).FullName
    }
}

function preparesys ()
{
    Write-Host "`nDeleting previous test vms`n"

    $deletevms = get-vm | Where-Object {($_.Name -like '*stresstest*') -or ($_.Name -like '*heaventest*') -or ($_.Name -like '*TestVM*') -or ($_.Name -like '*dynamicvf*')} | where-object {$_.State -like '*Running'}

    foreach ($deletevm in $deletevms)
    {
        Stop-VM -VM $deletevm -Force -Confirm:$false | Out-Null
    }

    Get-VM -Name *stresstest* | Remove-VM -Force
    Get-VM -Name *heaventest* | Remove-VM -Force
    Get-VM -Name *TestVM* | Remove-VM -Force
    Get-VM -Name *dynamicvf* | Remove-VM -Force

    $OSdrive = (Get-WmiObject Win32_OperatingSystem).SystemDrive
    [array] $drives = (Get-WmiObject win32_logicaldisk -filter "Drivetype = 3" | Select-Object deviceid | where-object{$_.deviceid -ne $OSdrive})

    ForEach($drive in $drives.deviceid) 
    { 
        $path = $drive + "\Program*"
        if (Test-path $path) 
        {
            $drives = $drives | ? {$_.deviceid -ne $drive}
        }
    }

    ForEach($drive in $drives) 
    { 
        $oldvmloc = (Get-ChildItem ($drive.deviceid + "\testvms") -recurse -Directory -ErrorAction SilentlyContinue | where-object {($_.Name -like '*stresstest*') -or ($_.Name -like '*heaventest*') -or ($_.Name -like '*TestVM*') -or ($_.Name -like '*dynamicvf*')}).FullName

        If (!([string]::IsNullOrEmpty($oldvmloc))) 
        {
            Remove-Item -Recurse -Force -Path $oldvmloc
        }
    }
}

Function PoweronVMs ([ValidateNotNullOrEmpty()] $testVMs)
{
    "`nPowering on VMs`n"

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    } elseif ( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($vm in $testVMs)
    {
        Start-VM -Name $vm -AsJob:$true -Confirm:$false
        sleep 2
    }
    
    foreach ($vm1 in $testVMs)
    {
        do {
            $status = Get-VM $vm1
            sleep 1
        } Until ($status.State -eq 'Running')        
    }
    sleep 2
}

Function Start-Guest
{
    param(
        [ValidateNotNull()] 
            [string] $vmname, 
        [ValidateRange(1, [int]::MaxValue)] 
            [int] $timeout
     )

    "`nPowering on VM: $vmname`n"

    Start-VM -Name $vmname -AsJob:$true -Confirm:$false -ErrorAction Stop
    
    $status = Get-VM -VMName $vmname
    $x = 0

    While ($x -le $timeout)
    {
        $x++
        sleep 1
        if (($status.State -eq 'Running')) 
        {
            "VM $vmname is now powered on`n"
            break
        }
    }
}

Function PrepRDP
{
    $rdppath = "HKCU:\Software\Microsoft\Terminal Server Client"
    $rdpwarningoverride = "AuthenticationLevelOverride"
    $overridevalue = "0"
    New-Item -Path $rdppath -ErrorAction SilentlyContinue
    New-ItemProperty -Path $rdppath -Name $rdpwarningoverride -Value $overridevalue -PropertyType DWORD -Force | Out-Null
}

function Avoidchkdsk
{
    $chkdskpath = "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager"
    $bootexecute = "BootExecute"
    $skipC = "autocheck autochk /q /v /k:C"
    New-ItemProperty -Path $chkdskpath -Name $bootexecute -Value $skipC -PropertyType MultiString -Force | Out-Null    
}

Function Wait-VMs
{
    param(
        [ValidateRange(1, [int]::MaxValue)]
            [int] $timeout, 
        [Alias('VMs')]
        [ValidateNotNullOrEmpty()]
            $testVMs
    )

    "`n`nWaiting for VMs to boot into OS`n"
    
    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs1 = Get-content $testVMs
    } elseif ( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Name
    } else
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Clone()
    }

    sleep 2

    foreach($vm in $testVMs1)
    {
        $vm4 = Get-VM $vm
        #$checkip = Get-VMNetworkAdapter -VM $vm4 | where {$_.IPAddresses -ne ''}
        #Wait-Tools -VM  $vm4 -TimeoutSeconds $interval

        $x = 0
        While ($x -le $timeout)
        {
            $x++
            sleep 1
            if (-not ([string]::IsNullOrEmpty((Get-VMNetworkAdapter $vm4).IPAddresses))) 
            {
                sleep 10
                if (-not ([string]::IsNullOrEmpty((Get-VMNetworkAdapter $vm4).IPAddresses)))
                {
                    break
                }
            }
        }

        if ([string]::IsNullOrEmpty((Get-VMNetworkAdapter $vm4).IPAddresses))
        {
            "`n$vm4 failed to boot into OS after $timeout seconds"
            "Exiting here"
            Exit
        }
    }

    "VMs have been fully booted in the OS`n"
    sleep 5

    $testVMs1.Clear()
}

Function WaitforOS
{
    param(
        [ValidateRange(1, [int]::MaxValue)]
            [int] $timeout, 
        [Alias('VM')]
        [ValidateNotNullOrEmpty()]
            [string] $vmname
    )

    "`nWaiting for VM to boot into OS"
     
    $vm4 = Get-VM -Name $vmname
    #$checkip = Get-VMNetworkAdapter -VM $vm4 | where {$_.IPAddresses -ne ''}

    $x = 0
    While ($x -le $timeout)
    {
        $x++
        sleep 1
        if (-not ([string]::IsNullOrEmpty((Get-VMNetworkAdapter $vm4).IPAddresses))) {break}
    }

    if ([string]::IsNullOrEmpty((Get-VMNetworkAdapter $vm4).IPAddresses))
    {
        "`n$vm4 failed to boot into OS after $timeout seconds"
        "Test Failed: Exiting here"
        Exit
    }

    "VM has been fully booted in the OS`n"
    sleep 4
}

function RPCsetup
{
    param(
        [ValidateNotNullOrEmpty()]
            $testVMs,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs1 = Get-content $testVMs
    } elseif ( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Name
    } else
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Clone()
    }


    foreach ($VM in $testVMs1)
    {           
        Enable-VMIntegrationService -VMName $VM -Name 'Guest Service Interface'
        Invoke-Command -VMName $VM -ScriptBlock {Set-ExecutionPolicy Unrestricted -Force} -Credential $cred 
        Invoke-Command -VMName $VM -ScriptBlock {winrm quickconfig -q -force} -Credential $cred -ErrorAction SilentlyContinue
        Invoke-Command -VMName $VM -ScriptBlock {Set-service RpcLocator -startup Automatic} -Credential $cred
        Invoke-Command -VMName $VM -ScriptBlock {Start-service RpcLocator} -Credential $cred
        Invoke-Command -VMName $VM -ScriptBlock {Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False} -Credential $cred 
        if ($VM -match 'W2019')
        {
            Invoke-Command -VMName $VM -ScriptBlock {Powercfg /hibernate ON} -Credential $cred
        }
    }
    $testVMs1.Clear()

}

function DisableVMRecovery
{
    param(
        [ValidateNotNullOrEmpty()]
            $testVMs,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs1 = Get-content $testVMs
    } elseif( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Name
    } else
    {
        [System.Collections.ArrayList] $testVMs1 = $testVMs.Clone()
    }

    foreach ($VM in $testVMs1)
    {           
        Invoke-Command -VMName $VM -ScriptBlock {bcdedit /set recoveryenabled No} -Credential $cred
        Invoke-Command -VMName $VM -ScriptBlock {bcdedit /set bootstatuspolicy ignoreallfailures} -Credential $cred
        Invoke-Command -VMName $VM -ScriptBlock ${function:Avoidchkdsk} -Credential $cred
    }
    $testVMs1.Clear()
}


Function Connect-Mstsc {

    [cmdletbinding(SupportsShouldProcess,DefaultParametersetName='UserPassword')]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Position=0)]
        [Alias('CN')]
            [string[]]     $ComputerName,
        [Parameter(ParameterSetName='UserPassword',Mandatory=$true,Position=1)]
        [Alias('U')] 
            [string]       $User,
        [Parameter(ParameterSetName='UserPassword',Mandatory=$true,Position=2)]
        [Alias('P')] 
            [string]       $Password,
        [Parameter(ParameterSetName='Credential',Mandatory=$true,Position=1)]
        [Alias('C')]
            [PSCredential] $Credential,
        [Alias('MM')]
            [switch]       $MultiMon,
        [Alias('F')]
            [switch]       $FullScreen,
        [Alias('W')]
            [int]          $Width,
        [Alias('H')]
            [int]          $Height
    )

    begin {
        [string]$MstscArguments = ''
        switch ($true) {
            {$MultiMon}   {$MstscArguments += '/multimon '}
            {$FullScreen} {$MstscArguments += '/f '}
            {$Width}      {$MstscArguments += "/w:$Width "}
            {$Height}     {$MstscArguments += "/h:$Height "}
        }

        if ($Credential) {
            $User     = $Credential.UserName
            $Password = $Credential.GetNetworkCredential().Password
        }
    }
    process {
        foreach ($Computer in $ComputerName) {
            $ProcessInfo = New-Object System.Diagnostics.ProcessStartInfo
            $Process = New-Object System.Diagnostics.Process
            
            if ($Computer.Contains(':')) {
                $ComputerCmdkey = ($Computer -split ':')[0]
            } else {
                $ComputerCmdkey = $Computer
            }

            $ProcessInfo.FileName    = "$($env:SystemRoot)\system32\cmdkey.exe"
            $ProcessInfo.Arguments   = "/generic:TERMSRV/$ComputerCmdkey /user:$User /pass:$($Password)"
            $ProcessInfo.WindowStyle = [System.Diagnostics.ProcessWindowStyle]::Hidden
            $Process.StartInfo = $ProcessInfo
            if ($PSCmdlet.ShouldProcess($ComputerCmdkey,'Adding credentials to store')) {
                [void]$Process.Start()
            }

            $ProcessInfo.FileName    = "$($env:SystemRoot)\system32\mstsc.exe"
            $ProcessInfo.Arguments   = "$MstscArguments /v $Computer"
            $ProcessInfo.WindowStyle = [System.Diagnostics.ProcessWindowStyle]::Normal
            $Process.StartInfo       = $ProcessInfo
            if ($PSCmdlet.ShouldProcess($Computer,'Connecting mstsc')) {
                [void]$Process.Start()
                if ($Wait) {
                    $null = $Process.WaitForExit()
                }       
            }
        }
    }
}

function Get-GPU
{
    $gpus = New-Object System.Collections.ArrayList
    $listofgpus = Get-VMPartitionableGpu

    foreach($gpu in $listofgpus)
    {
        if(($gpu | Where-Object {$_.name -notmatch "GPUPARAV"}).Name)
        {
            $gpus += $gpu
        }
    }

    if($gpus.Count -eq 0)
    {
        Write-Host "No compatible GPU found" -BackgroundColor Red
    }

    for ($ind_gpu=0; $ind_gpu -lt $gpus.Count; $ind_gpu++) 
    {
        "`nGPU #$($ind_gpu+1) is configured to $($gpus[$ind_gpu].PartitionCount) VF"    
    }
}

function CheckRunningVMs()
{

    $checkvmsstate = Get-VM * | Where {$_.State -like '*Running*'}

    If ($checkvmsstate) 
    {
        Write-Host "`nSome VMs are still running. May need to shut them down before continuing`n"

        [string]$on_select = Read-Host -Prompt "Do you want them to be shut down (y/n)"

        If ($on_select -like 'y*') 
        {
            foreach ($VM in $checkvmsstate)
            {
                Stop-VM -VM $VM -AsJob:$true -Confirm:$false
                sleep 2
            }

            $vmturningoff = Get-VM * | Where {$_.State -like '*Running*'}

            $curTime = Get-Date
            $check = $false

            foreach ($vm_on in $vmturningoff)
            {

                While ((((Get-Date) - $curTime).totalseconds -lt '150') -and !$check)
                {
                    if($vm_on.State -eq "Off")
                    {
                        $check = $true
                    }
                }

                if(!$check)
                {
                    "`n$vm_on failed to turned off after 150 seconds`n"
                }
            }

        } elseif ($on_select -like 'n*') {

            Write-Host "Will try to assign any available vf to the test vms"
            
        } else {
                
            Write-Host "Invalid option"
            exit
        }
    }
}

function Set-GPUPartition()
{

    $availablegpus = (Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | measure).Count

    $vfconfig = Read-Host "`nSpecify GPU Partition:"

    $selectoption = Read-host "`nAll GPUs to be configured: (y/n)"

    If ($selectoption -like 'n*')
    {
        Write-Host "adapternum: Select which gpu adapter id to do the assignment" "(Available: $availablegpus)"
        $adapternum = Read-Host "`nSpecify which GPU slot:"
        $selected_adapter = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"} | Select-Object -first $adapternum | Select-Object -last 1
    } else {
        
        $selected_adapter = Get-VMPartitionableGpu | Where-Object {$_.name -notmatch "GPUPARAV"}
    }

    CheckRunningVMs
			
    foreach ($gpu in $selected_adapter) 
    {
        $gpuid = $gpu.Name | format-table -HideTableHeaders | Out-String
	    Set-VMPartitionableGpu -Name $gpuid.trim() -PartitionCount $vfconfig -ErrorAction Stop				
    }

    Get-GPU
}

function Get-VMInfo
{
    param([string] $servername)

    $vms = Get-VM -ComputerName $servername
    $vmInfo = New-Object System.Collections.ArrayList

    $vf1size = "16861102080"
    $vf2size = "8422162432"
    $vf4size = "4211081216"
    $vf8size = "2097152000"

    Foreach ($vm in $vms) 
    {
        $gpuinfo = Get-VMGpuPartitionAdapter -ComputerName $servername -VMName $vm.Name -errorAction SilentlyContinue
        $vmmem = [math]::round((Get-VMMemory -ComputerName $servername -VMName $vm.Name).Startup/1Gb,3)
        $vmcpu = (Get-VMProcessor -ComputerName $servername -VMName $vm.Name).Count
        $vmip = (Get-VMNetworkAdapter -ComputerName $servername -VMName $vm.Name).IPAddresses| ?{$_ -notmatch ':'} | Select-Object -last 1

        if($gpuinfo)
        {
            if ($gpuinfo.MinPartitionVRAM -eq $vf1size)
            {
                $hash=[ordered]@{Name=$vm.Name; State=$vm.State; Memory=$vmmem; vCPU=$vmcpu; IPv4=$vmip; GPUPartition='VF_1'}
                $vmInfo += New-Object -TypeName psobject -Property $hash

            } elseif ($gpuinfo.MinPartitionVRAM -eq $vf2size) {

                $hash=[ordered]@{Name=$vm.Name; State=$vm.State; Memory=$vmmem; vCPU=$vmcpu; IPv4=$vmip; GPUPartition='VF_2'}
                $vmInfo += New-Object -TypeName psobject -Property $hash

            } elseif ($gpuinfo.MinPartitionVRAM -eq $vf4size) {

                $hash=[ordered]@{Name=$vm.Name; State=$vm.State; Memory=$vmmem; vCPU=$vmcpu; IPv4=$vmip; GPUPartition='VF_4'}
                $vmInfo += New-Object -TypeName psobject -Property $hash
            } elseif ($gpuinfo.MinPartitionVRAM -eq $vf8size) {

                $hash=[ordered]@{Name=$vm.Name; State=$vm.State; Memory=$vmmem; vCPU=$vmcpu; IPv4=$vmip; GPUPartition='VF_8'}
                $vmInfo += New-Object -TypeName psobject -Property $hash
            }
        } else {
          $hash=[ordered]@{Name=$vm.Name; State=$vm.State; Memory=$vmmem; vCPU=$vmcpu; IPv4=$vmip; GPUPartition='No GPU'}
          $vmInfo += New-Object -TypeName psobject -Property $hash
        }
    }                       
    
    $vmInfo | Format-Table -Property Name,IPv4,@{n="State";expression={$_.State};a="left"},@{n="vCPU";expression={$_.vCPU};a="left"},@{n="Memory";expression={$_.Memory};a="left"},GPUPartition
    $vmInfo.clear()
}

function WaitforVMoff()
{
    param(
        [Alias('VMList')]
        [ValidateNotNullOrEmpty()]
            $vmarray,
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime,
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait
    )

    $checkpath = Test-Path $vmarray

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $vmarray1 = Get-content $vmarray
    } elseif( $vmarray.Name -ne $null)
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Name
    } else
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Clone()
    }

    "`nVerifying VMs`n"

    $starttime = (get-date).TimeOfDay
    do
    {
        sleep $waittime
        
            for ($i = 0; $i -lt $vmarray1.count; $i++)
            {
                if ((Get-VM -Name $vmarray1[$i]).state -eq "Off")
                {
                    $vmoff = $vmarray1[$i]
                    "$vmoff is now off"
                    $vmarray1.RemoveAt($i)
                    $i--
                } else {
                    continue
                }
            }

    } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime).seconds -lt $maxwait))

    if ($vmarray1.count -gt 0)
    {
        "`nWaited $maxwait secs - some VMs are stuck, will initiate vm shutdown one more time"

        foreach ($RunVM in $vmarray1)
        {
            if (!((Get-VM -Name $RunVM).state -eq "Off"))
            {
                Stop-VM -Name $RunVM -AsJob:$true -Confirm:$false -Force
                "$RunVM to shutdown one more time" 
            }
        }

        "Checking problematic VMs"
        $starttime2 = (get-date).TimeOfDay
        do
        {
            sleep $waittime        
    
                for ($j = 0; $j -lt $vmarray1.count; $j++)
                {
                    if ((Get-VM -Name $vmarray1[$j]).state -eq "Off")
                    {
                        $vmarray1.RemoveAt($j)
                        $j--
                    }
                    else
                    {
                        continue
                    }
                }

        } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime2).seconds -lt $maxwait))
    }

    if ($vmarray1.count -gt 0)
    {
        foreach ($RunVM2 in $vmarray1)
        {
            "$RunVM2 is still stuck"
        }

        $vmarray1.Clear()
        "Failed: Exiting here"
        Exit
    } else
    {
        "`nAll VMs are now off`n"
    }
}

function WaitforVMHibernation()
{
    param(
        [Alias('VMList')]
        [ValidateNotNullOrEmpty()]
            $vmarray,
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime,
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait
    )

    $checkpath = Test-Path $vmarray

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $vmarray1 = Get-content $vmarray
    } elseif( $vmarray.Name -ne $null)
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Name
    } else
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Clone()
    }

    "Verifying VMs`n"

    $starttime = (get-date).TimeOfDay
    do
    {
        sleep $waittime

            for ($i = 0; $i -lt $vmarray1.count; $i++)
            {
                if ((Get-VM -Name $vmarray1[$i]).state -eq "Hibernated")
                {
                    $vmhib = $vmarray1[$i]
                    "$vmhib has hibernated"
                    $vmarray1.RemoveAt($i)
                    $i--
                } else {
                    continue
                }
            }

    } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime).seconds -lt $maxwait))

    if ($vmarray1.count -gt 0)
    {
        "`nWaited $maxwait secs - some VMs are stuck, will initiate vm hibernation one more time"

        foreach ($RunVM in $vmarray1)
        {
            $vm1 = Get-VM $RunVM
            if ($vm1.state -eq "Running") 
            {
                Invoke-Command -VMName $vm1.Name -ScriptBlock {shutdown -h} -Credential $cred -ErrorAction SilentlyContinue
                "$RunVM to hibernate one more time" 
            }
        }

        "Checking problematic VMs"

        $starttime2 = (get-date).TimeOfDay
        do
        {
            sleep $waittime
    
                for ($j = 0; $j -lt $vmarray1.count; $j++)
                {
                    if ((Get-VM -Name $vmarray1[$j]).state -eq "Hibernated")
                    {
                        $vmarray1.RemoveAt($j)
                        $j--
                    }
                    else
                    {
                        continue
                    }
                }

        } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime2).seconds -lt $maxwait))
    }

    if ($vmarray1.count -gt 0)
    {
        foreach ($RunVM2 in $vmarray1)
        {
            "$RunVM2 is still stuck"
        }

        $vmarray1.Clear()
        "Failed: Exiting here"
        Exit
    } else
    {
        "`nAll VMs have now hibernated`n"
    }
}

function WaitforVMreboot()
{
    param(
        [Alias('VMList')]
        [ValidateNotNullOrEmpty()]
            $vmarray,
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime,
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait
    )

    $checkpath = Test-Path $vmarray

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $vmarray1 = Get-content $vmarray
    } elseif( $vmarray.Name -ne $null)
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Name
    } else
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Clone()
    }

    "Verifying if all VMs have rebooted`n"
    $starttime = (get-date).TimeOfDay
    do
    {
        for ($i = 0; $i -lt $vmarray1.count; $i++)
        {
            if ((Get-VM -Name $vmarray1[$i]).Uptime.TotalSeconds -le 60)
            {
                $vmre = $vmarray1[$i]
                "$vmre has rebooted"
                $vmarray1.RemoveAt($i)
                $i--
            } else {
                continue
            }
        }

    } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime).seconds -lt $maxwait))

    if ($vmarray1.count -gt 0)
    {
        "`nWaited $maxwait secs - some VMs may be stuck, will initiate vm reboot one more time"

        foreach ($RunVM in $vmarray1)
        {
            $vm1 = Get-VM $RunVM
            if ($vm1.Uptime.TotalSeconds -gt ($maxwait + 5)) 
            {
                Invoke-Command -VMName $vm1.Name -ScriptBlock {shutdown /r /t 0} -Credential $cred -ErrorAction SilentlyContinue
                "$RunVM to reboot one more time" 
            }
        }

        "Checking problematic VMs"
        $starttime2 = (get-date).TimeOfDay
        do
        {
            sleep $waittime

            for ($j = 0; $j -lt $vmarray1.count; $j++)
            {
                if ((Get-VM -Name $vmarray1[$j]).Uptime.TotalSeconds -le 45)
                {
                    $vmarray1.RemoveAt($j)
                    $j--
                }
                else
                {
                    continue
                }
            }

        } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime2).seconds -lt $maxwait))
    }

    if ($vmarray1.count -gt 0)
    {
        foreach ($RunVM2 in $vmarray1)
        {
            "$RunVM2 is still stuck"
        }

        $vmarray1.Clear()
        "Failed: Exiting here"
        Exit
    } else
    {
        "`nAll VMs have now fully rebooted`n"
    } 
    sleep 2
}

function WaitandPowerON()
{
    param(
        [Alias('VMList')]
        [ValidateNotNullOrEmpty()]
            $vmarray,
        [Alias('Interval')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $waittime,
        [Alias('Timeout')]
        [ValidateRange(1, [int]::MaxValue)]
            [int] $maxwait
    )

    $vm_to_poweron = new-object system.collections.arraylist

    $checkpath = Test-Path $vmarray

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $vmarray1 = Get-content $vmarray
    } elseif( $vmarray.Name -ne $null)
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Name
    } else
    {
        [System.Collections.ArrayList] $vmarray1 = $vmarray.Clone()
    }

    "`nVerifying VMs`n"
    $starttime = (get-date).TimeOfDay
    do
    {
        sleep $waittime
        
            for ($i = 0; $i -lt $vmarray1.count; $i++)
            {
                if ((Get-VM -Name $vmarray1[$i]).state -eq "Off")
                {
                    $vm_to_poweron.Add($vmarray1[$i]) | Out-Null
                    $vmoff = $vmarray1[$i]
                    "$vmoff is now off"
                    $vmarray1.RemoveAt($i)
                    $i--
                    
                } elseif((Get-VM -Name $vmarray1[$i]).state -eq "Hibernated")
                {
                    $vm_to_poweron.Add($vmarray1[$i]) | Out-Null
                    $vmoff = $vmarray1[$i]
                    "$vmoff has hibernated"
                    $vmarray1.RemoveAt($i)
                    $i--

                }
            }

    } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime).seconds -lt $maxwait))

    if ($vmarray1.count -gt 0)
    {
        "`nWaited $maxwait secs - some VMs are stuck, will shutdown one more time. If supposed to be hibernated, then that VM will undergo it next cycle"

        foreach ($RunVM in $vmarray1)
        {
            if (!((Get-VM -Name $RunVM).state -eq "Off"))
            {
                Stop-VM -Name $RunVM -AsJob:$true -Confirm:$false -Force
                "$RunVM to shutdown one more time" 
            }
        }

        "Checking problematic VMs"
        $starttime2 = (get-date).TimeOfDay
        do
        {
            sleep $waittime        
    
                for ($j = 0; $j -lt $vmarray1.count; $j++)
                {
                    if ((Get-VM -Name $vmarray1[$j]).state -eq "Off")
                    {
                        $vm_to_poweron.Add($vmarray1[$j]) | Out-Null
                        $vmarray1.RemoveAt($j)
                        $j--
                    }
                    else
                    {
                        continue
                    }
                }

        } while (($vmarray1.count -gt 0) -or (((get-date).TimeofDay - $starttime2).seconds -lt $maxwait))
    }

    if ($vmarray1.count -gt 0)
    {
        foreach ($RunVM2 in $vmarray1)
        {
            "$RunVM2 is still stuck"
        }
        "Failed: Exiting here"
        Exit
    } else
    {
        "`nAll VMs is now off or hibernated`n"
    }

    $vmarray1.Clear()
    sleep 5

    "`nPowering on specific VMs`n"
    foreach ($vm in $vm_to_poweron)
    {
        Start-VM -Name $vm -AsJob:$true -Confirm:$false
        sleep 2
    }

    $vm_to_poweron.Clear()
}

function ShutdownVMs([ValidateNotNullOrEmpty()] $testVMs)
{
    "`nShutting down VMs`n"

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($vm in $testVMs)
    {
        Stop-VM -Name $vm -Confirm:$false -Force
        sleep 2
    }
}

function HibernateVMs([ValidateNotNullOrEmpty()] $testVMs, [PSCredential] $cred)
{
    "`nHibernating VMs`n"

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($vm in $testVMs)
    {
        Invoke-Command -VMName $vm -ScriptBlock {shutdown -h} -Credential $cred
        sleep 2
    }
}

function RebootVMsfromGuest([ValidateNotNullOrEmpty()] $testVMs, [PSCredential] $cred)
{
    "`nRestarting VMs from Guest`n"

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($vm in $testVMs)
    {
        Invoke-Command -VMName $vm -ScriptBlock {shutdown /r /t 0} -Credential $cred
        sleep 2
    }
}

function RebootVMsfromHost([ValidateNotNullOrEmpty()] $testVMs)
{
    "`nRestarting VMs from Host`n"

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($vm in $testVMs)
    {
        $vmip = (Get-VMNetworkAdapter -VMName $vm).IPAddresses | select -first 1
            
        If (-not ([string]::IsNullOrEmpty($vmip)))
        {
            Restart-Computer -ComputerName $vmip -Force -ErrorAction SilentlyContinue
            sleep 2
        }
    }
}

function WaitforSysprep([ValidateNotNullOrEmpty()] $testVMs)
{
    PoweronVMs $testVMs
    sleep 170
    Wait-VMs -timeout "900" -VMs $testVMs
    sleep 10
    ShutdownVMs $testVMs
    WaitforVMoff -VMList $testVMs -Interval "2" -Timeout "600"

}

function Get-AMDVFDevice()
{
    param(
        [ValidateNotNull()] 
            [String]$vmname,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )
    
    "Checking for valid VF devices"
    $ip = (Get-VmNetworkAdapter $vmname | select -ExpandProperty IPAddresses)[0]

    Start-sleep 2

    do
    {
        $driverObject = Get-WmiObject win32_pnpentity -ComputerName $ip -Credential $cred -ErrorAction SilentlyContinue -ErrorVariable failure | Where-Object{$_.pnpclass -eq 'Display'-and $_.PNPDeviceID -match 'PCI\\VEN_1002.'}
    } Until ($driverObject -ne $null)

    Start-sleep 2
    $driverObject = Get-WmiObject win32_pnpentity -ComputerName $ip -Credential $cred -ErrorAction Stop | Where-Object{($_.pnpclass -eq 'Display'-and $_.PNPDeviceID -match 'PCI\\VEN_1002.') -or  $_.PNPDeviceID -match 'PCI\\VEN_1002&DEV_686C.'}                    
    
    if(!$driverObject)
    {
        throw "!!!Error, Device not found in $vmane. Please make sure a valid VF is assigned to the VM"
        Exit
    } else
    {
        "Found AMD Device in $vmname"
    }

    $errorCode = $driverObject | select -expand Service

    if($errorCode -ne 'BasicDisplay'){
        throw "!!!Error! Device returned with service name $errorCode"
        Exit
    } else {
        "`nDevice ready for install on $vm_on`n"
    }

}

function Copy-AMDDriver()
{
    param(
        [Alias('VM')]
        [ValidateNotNullOrEmpty()]
            [string] $vm,
        [Alias('DriverLocation')]
        [ValidateNotNullOrEmpty()]
            [string] $driverlocation,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    "`nCopying driver to $vm`n"

    Invoke-Command -VMName $vm -ScriptBlock {param($driverlocation) Copy-Item $driverlocation -Destination C:\AMDDriver\ -Recurse} -Credential $cred -ArgumentList $driverlocation
}

function Install-AMDDriver()
{
    param(
        [Alias('VM')]
        [ValidateNotNullOrEmpty()]
            [string] $vm,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    "`nChecking setup folder`n"
    $SetupPath = Invoke-Command -VMName $vm -ScriptBlock {Get-ChildItem "C:\AMDDriver\" -Recurse | ?{$_.Name -eq "Setup.exe"} | select -expand fullname} -Credential $cred
    
    if($SetupPath.count -eq 0)
    {
         Write-Error "Setup executable not found under C:\AMDDriver\"
         Exit
    }
    elseif($SetupPath.count -ne 1)
    {
        $InstallPath = $SetupPath[0]
    }
    else
    {  
       $InstallPath = $SetupPath
    }

    "`nInstalling AMD Driver in $vm`n"
    Invoke-Command -VMName $vm -ScriptBlock {param($InstallPath)
        $InstallProcess = Start-Process -FilePath "$InstallPath" -ArgumentList '-Install' -Verb RunAs -PassThru 
        $processID = $InstallProcess.Id
        While((Get-Process -id $processID))
        {
            "`nWaiting for driver install to complete`n"
            sleep 10
        }
    } -Credential $cred -ArgumentList $InstallPath

}

function CheckVHD
{
    param(
        [ValidateNotNullOrEmpty()]
            [string] $vmbaselocation,
        [ValidateSet("1", "2", "4", "8")]
            [int] $vfconfig  
    )

    $size = (Get-VHD $vmbaselocation).Size/1gb -as [int]

    If ($vfconfig -eq 1)
    {
        if($size -ge 150)
        {
            Write-Host "VM VHD Size is good for Hibernation test"
        } else
        {
            Write-Host "Please increase VM VHD Size to, at least, 150GB"
            Exit
        }
    } elseif ($vfconfig -eq 2) {

        if($size -ge 90)
        {
            Write-Host "VM VHD Size is good for Hibernation test"
        } else
        {
            Write-Host "Please increase VM VHD Size to, at least, 90GB"
            Exit
        }
    } elseif ($vfconfig -eq 4) {

        if($size -ge 65)
        {
            Write-Host "VM VHD Size is good for Hibernation test"
        } else
        {
            Write-Host "Please increase VM VHD Size to, at least, 65GB"
            Exit
        }
    } elseif ($vfconfig -eq 8) {

        if($size -ge 50)
        {
            Write-Host "VM VHD Size is good for Hibernation test"
        } else
        {
            Write-Host "Please increase VM VHD Size to, at least, 50GB"
            Exit
        }
    } else {
        
        Write-Host "Wrong VF input"
        Exit
    }
}


function CheckTDR
{
    param(
        [datetime] $whencheck,
        [ValidateNotNull()] 
            [String]$testvm,
        [Alias('Credential')]
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    #$whencheck = Get-Date -Date '1/17/2019 08:00:00'
    $tdrcheck = Invoke-command -VMName $testvm -ScriptBlock{Param($whencheck) Get-EventLog -LogName System -Message *amdkmdap*stopped* -after $whencheck} -Credential $cred -ArgumentList $whencheck

    $hosttdrcheck = Get-EventLog -LogName System -Message "*source*amdkmdag*" -after $whencheck
    
    if($tdrcheck -ne $null){
        $errorcode = 1
        throw "TDR happened on $testvm at $($tdrcheck[0].TimeWritten). Exiting here"
        
        if($hosttdrcheck -ne $null)
        {
            throw "TDR event on host at $($hosttdrcheck[0].TimeWritten). Exiting here"
        }
        Exit
    } else {

        if($hosttdrcheck -ne $null)
        {
            throw "TDR event on host at $($hosttdrcheck[0].TimeWritten). Exiting here"
            Exit
        }
        #"Good! No TDR on $testvm"
        $errorcode = 0
    }

    return $errorcode
}

Function CheckTDRonAllGuests
{
    param(
        [datetime] $whencheck,
        [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs,
        [Alias('Credential')]
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    Foreach ($vm in $testVMs)
    {
        $errorcode = CheckTDR -whencheck $whencheck -testvm $vm -cred $cred
        
        if($errorcode -ne 0){
            throw "TDR happened on $vm. Exiting here"
        } else {
            "Good! No TDR on $vm"
        }
    sleep 2
    }
}

Function CheckAllGuestDriver
{
    param(
        [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs,
        [Alias('Credential')]
        [ValidateNotNull()] 
            [PSCredential]$cred
    )


    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If( $testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    Foreach ($vm in $testVMs)
    {
        $errorCode = Get-GuestDriverStatus -vmname $vm -cred $cred
        if($errorCode -ne 0){
            throw "!!!Error! Driver returned errorCode $errorCode"
        } else {
            "Driver is running properly on $vm"
        }
    sleep 2
    }
}

function Get-GuestDriverStatus
{
    param(
        [ValidateNotNull()] 
            [String]$vmname,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    #$ip = (Get-VmNetworkAdapter $vmname | select -ExpandProperty IPAddresses)[0]

    do
    {
        $driverObject = Invoke-Command -VMName $vmname -ScriptBlock{Get-WmiObject win32_pnpentity -ErrorAction SilentlyContinue -ErrorVariable failure | Where-Object{$_.pnpclass -eq 'Display'-and $_.PNPDeviceID -match 'PCI\\VEN_1002.'}} -Credential $cred
    } Until ($driverObject -ne $null)

    sleep 1
    $driverObject = Invoke-Command -VMName $vmname -ScriptBlock{Get-WmiObject win32_pnpentity -ErrorAction Stop | Where-Object{($_.pnpclass -eq 'Display'-and $_.PNPDeviceID -match 'PCI\\VEN_1002.') -or  $_.PNPDeviceID -match 'PCI\\VEN_1002&DEV_686C.'}} -Credential $cred                  
    
    if(!$driverObject)
    {
        throw "!!!Error, Device not found in $vmname. Please make sure a valid VF is assigned to the VM"
    }
    $errorCode = $driverObject | select -expand ConfigManagerErrorCode

    return $errorCode
}

function AddInternalNetwork
{
    param(
    [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    $int_switch = Get-VMSwitch | where{$_.SwitchType -like '*internal*'}

    If (!$int_switch) 
    {
        New-VMSwitch -Name NAT -SwitchType Internal
    }

    foreach ($VM in $testVMs) 
    {
        $checknat = Get-VMNetworkAdapter -VMName $VM | where {$_.SwitchName -like '*NAT'}

        If (!$checknat) 
        {   
            Add-VMNetworkAdapter -VMName $VM -SwitchName NAT
        }
    }
}

function CopyHeavenResultstoserver
{
    param(
    [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($VM in $testVMs) 
    {
        Invoke-Command -VMName $VM -ScriptBlock {
            $ip = (Get-NetIPAddress -AddressFamily IPv4 | where {($_.InterfaceAlias -notmatch 'Loopback') -and ($_.PrefixOrigin -like 'Dhcp')}).IPAddress | select -first 1
            new-item -itemtype directory -force -path \\loginvsi.amd.com\logs\azure\Automated_Results\vfconfig\$ip
            Copy-Item -Path C:\Heaven\api_run.csv -Destination \\loginvsi.amd.com\Logs\Azure\Automated_Results\vfconfig\$ip\dynamic_vf_test.csv -Force
        } -credential $cred
    }

}

function CopyHeaven
{
    param(
        [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs,
        [ValidateNotNull()] 
            [PSCredential]$cred,
        [ValidateNotNull()] 
            [String]$heavenlocation,
        [ValidateNotNull()] 
            [int]$unzipped
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($heavenVM in $testVMs) 
    {
        $checktools = Invoke-Command -VMName $heavenVM -ScriptBlock {[System.IO.File]::Exists("C:\PSTools\PSExec64.exe")} -credential $cred

        If (!$checktools) 
        {
            Copy-VMFile -Name $heavenVM -SourcePath "$root\PSTools.zip" -DestinationPath "C:\PSTools\PSTools.zip" -CreateFullPath -FileSource Host -ErrorAction Stop
            Invoke-Command -VMName $heavenVM -ScriptBlock {Expand-Archive C:\PSTools\PSTools.zip -DestinationPath C:\PSTools\} -Credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {Remove-Item -path C:\PSTools\PSTools.zip} -Credential $cred
            Invoke-Command -VMName $heavenVM -ScriptBlock {
                $oldPath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path
                $newPath = $oldPath + ';C:\PSTools\'
                Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -value $newPath
            } -Credential $cred

        }

        Sleep 2

        $checkheaven = Invoke-Command -VMName $heavenVM -ScriptBlock {[System.IO.File]::Exists("C:\Heaven\unins000.exe")} -credential $cred

        if (!$checkheaven) 
        {
            Invoke-Command -VMName $heavenVM -ScriptBlock {cmdkey /add:loginvsi /user:amd\taccuser /pass:AH64_uh1} -Credential $cred
            Sleep 2

            if ($unzipped -ne 1)
            {
                Copy-VMFile -Name $heavenVM -SourcePath $heavenlocation -DestinationPath "C:\Heaven.zip" -CreateFullPath -FileSource Host
                Invoke-Command -VMName $heavenVM -ScriptBlock {Expand-Archive C:\Heaven.zip -DestinationPath C:\} -Credential $cred
                Invoke-Command -VMName $heavenVM -ScriptBlock {Remove-Item -path C:\Heaven.zip} -Credential $cred
                Copy-VMFile -Name $heavenVM -SourcePath $root\core\heaven_run.ps1 -DestinationPath "C:\Heaven\heaven_run.ps1" -CreateFullPath -FileSource Host
            } else {
                Get-ChildItem $heavenlocation -Recurse -File | foreach { 
                    $path = ($_.DirectoryName + "\") -Replace [Regex]::Escape($heavenlocation), "C:\Heaven"
                    $path = Join-Path -Path $path -ChildPath $_.Name
                    Copy-VMFile -Name $heavenVM -SourcePath $_.FullName -DestinationPath $path -CreateFullPath -FileSource Host}
                Copy-VMFile -Name $heavenVM -SourcePath $root\core\heaven_run.ps1 -DestinationPath "C:\Heaven\heaven_run.ps1" -CreateFullPath -FileSource Host -ErrorAction SilentlyContinue
            }
        }

        Invoke-Command -VMName $heavenVM -ScriptBlock {Set-ItemProperty 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\' -Name "fDenyTSConnections" -Value 0} -Credential $cred
        Invoke-Command -VMName $heavenVM -ScriptBlock {Set-ItemProperty 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -Name "UserAuthentication" -Value 1} -Credential $cred
        Invoke-Command -VMName $heavenVM -ScriptBlock {Enable-NetFirewallRule -DisplayGroup "Remote Desktop"} -Credential $cred
    }
}

function RDPConnect
{
    param(
        [Alias('VMs')]
        [ValidateNotNull()] 
            $testVMs,
        [ValidateNotNull()] 
            [PSCredential]$cred
    )

    $checkpath = Test-Path $testVMs

    If ($checkpath -eq $true)
    {
        [System.Collections.ArrayList] $testVMs = Get-content $testVMs
    }

    If($testVMs.Name -ne $null)
    {
        [System.Collections.ArrayList] $testVMs = $testVMs.Name
    }

    foreach ($VM in $testVMs) {

        $getip = ((Get-VMNetworkAdapter -VMName $VM).IPAddresses) | Select-Object -first 1

        Connect-Mstsc -CN $getip -Credential $cred -Width 1920 -Height 1080
        Sleep 2
    }
}

#Mod from Aaron
function Get-AmdDevices
{
    param(
        $deviceIds = @('PCI\\VEN_1002')
    )
    $devices = @()
    foreach($deviceId in $deviceIds)
    {
        $device = Get-WmiObject win32_pnpentity  -ErrorAction Stop  | Where-Object{$_.pnpclass -eq 'Display'-and $_.PNPDeviceID -match $deviceId}
        $devices += $device
    }    
    if(!$devices)
    {
        throw "!!!Error, Devices not found"
    } 
    return $devices
}

function TestHostDriver
{
    $devices = Get-AmdDevices
    
    foreach($device in $devices)
    {
        if($device.ConfigManagerErrorCode -ne 0)
        {
            throw "!!!Error, $($device.PNPDeviceID) returned errorCode $($device.ConfigManagerErrorCode)"
        }
    } 
}

function TestHostHyperVFeature
{
    if(!(get-windowsfeature -name "Hyper-V" | select -ExpandProperty Installed))
    {
        throw "!!!Error, Hyper-V featuer is not installed"
    }
    if(!(get-windowsfeature -name "Hyper-V-PowerShell" | select -ExpandProperty Installed))
    {
        throw "!!!Error, Hyper-V-PowerShell is not installed"
    }
}

function TestCrashBehaviour
{
    if(Get-WmiObject Win32_OSRecoveryConfiguration -EnableAllPrivileges | select -expand autoReboot)
    {
        Write-Host "Disabling AutoReboot before continuing"
        Get-WmiObject Win32_OSRecoveryConfiguration -EnableAllPrivileges | Set-WmiInstance -Arguments @{ AutoReboot=$False }
    }

    if(!((Get-WmiObject Win32_OSRecoveryConfiguration -EnableAllPrivileges | select -expand DebugInfoType) -eq 2))
    {
        Write-Host "Setting dump creation to Kernel dump"
        Get-WmiObject Win32_OSRecoveryConfiguration -EnableAllPrivileges | Set-WmiInstance -Arguments @{ DebugInfoType=2 }
    }
}

function TestHostEnv
{
    TestHostDriver
    TestHostHyperVFeature
    TestCrashBehaviour
}

﻿Stop-Transcript -ErrorAction SilentlyContinue | Out-Null
Get-Variable -Exclude PWD,*Preference,PS* | Remove-Variable -ErrorAction SilentlyContinue | Out-Null
Remove-Job *
Clear-Host


$global:root = (Get-Item $PSScriptRoot).FullName
$global:mydoc = [environment]::getfolderpath("mydocuments")
$testVMs = "$mydoc\testvms.txt"
$pslogdir = "$mydoc\ps_log"
$time = Get-Date -format g | foreach {$_ -replace ":", "_"} | foreach {$_ -replace "/", "_"} | foreach {$_ -replace " ", "__"}
$logsession = $pslogdir + "\full_log_" + $time +".txt"
$priorcheck = "$env:TEMP\cpumodel.txt"

$checktemp = Test-Path $testVMs -IsValid

If ($checktemp -eq $true)
{
    Remove-item -Path $testVMs -Force -ErrorAction SilentlyContinue
}

Remove-Module azure_func -ErrorAction SilentlyContinue
Import-Module $root\core\azure_func.psm1

if (Test-Path $priorcheck)
{
    Clear-Host
    $cpucheck = Get-Content $priorcheck
    $cpucheck
    If ($cpucheck -match "^ROME server with 2 sockets - 64 cores each$")
    {
        $global:rome64flag = 1
    } else
    {
        $global:rome64flag = 0
    }

} else
{
    Clear-Host
    hostcheck
}

if ($rome64flag -eq 1)
{
    envcheck
}

"`nTasks to run:`n"
"1: Retrieve VMs Info"
"2: VM Cloning"
"3: Check GPU Partition"
"4: Set GPU Partition"
"5: VF\VM Assignment`n"
"6: Cold Boot from Host"
"7: Warm Boot from Guest"
"8: Cold Boot from Guest"
"9: VM Reset"
"10: VM Poweroff"
"11: Heaven API (DX11, DX9, OGL)"
"12: Dynamic VF Allocation"
"13: Host Driver PnP"
"14: Single VM Hibernation"
"15: VM Hibernation"
"16: VM Mix Power States"
"17: Force App Kill"


Write-Host "`n`nSelect one of the above to run`n"
[array]$fulltestarray = 1..17
[ValidateRange(1,17)][int]$selection = Read-Host -Prompt "Enter your selection [1-$($fulltestarray.Length)]: "


if ($fulltestarray.Contains($selection)) {
    Write-host "`nSelecting test case # $selection`n"
} else {
    Read-Host "`nInvalid test case num. Exiting..`n"
    Exit
}

Clear-Host

$tier1array = 6..17

if ($tier1array.Contains($selection)) {
    preparesys
    cmdkey /add:loginvsi /user:amd\taccuser /pass:AH64_uh1 | Out-Null
    Start-Transcript -Path $logsession -Force -IncludeInvocationHeader | Out-Null
}

$tier2array = 1,3,4,5

if (!($tier2array.Contains($selection))) {
    checktemploc
}


Invoke-expression "$root\core\test_launcher.ps1 $selection"
